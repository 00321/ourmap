<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Insert title here</title>
<!-- jQuery -->
<script
  src="https://code.jquery.com/jquery-3.4.1.min.js"
  integrity="sha256-CSXorXvZcTkaix6Yvo6HppcZGetbYMGWSFlBw8HfCJo="
  crossorigin="anonymous"></script> 
<!-- jQuery END-->
<link rel="stylesheet" href="/css/bootstrap.min.css">
<script src="/js/bootstrap.js"></script>
<script src="/js/bootstrap.min.js"></script>
<!-- fontawsome -->
<script src="https://kit.fontawesome.com/86fb79723e.js"
	crossorigin="anonymous"></script>
<!-- fontawsome end -->
<link rel="stylesheet" href="/css/pinInput.css">
<link rel="stylesheet" href="/icomoon/style.css">
</head>
<body>
	<div class="container">
		<div class="overlayboxinput">
			<div class="placeName">
				<label id="whiteCol">
					작성양식
				</label>
				<input type="text" class="form-control" id="basic-url"
					aria-describedby="basic-addon3" placeholder="제목">
			</div>
			<div class="first">
				<div class="center-block">
					<div class="triangle text">
						<div class="filebox">
							<img id="LoadImg" width="277px" height="150px">
							<input type="file" id="imgAttach" name="imgAttach" onchange="LoadImg();">
							<span>이미지 업로드</span>
						</div>
					</div>
				</div>
			</div>
			<button id="catBt">카테고리 <i class="fas fa-chevron-down"></i></button>
			<button id="catBtup" style="display:none">카테고리 <i class="fas fa-chevron-up"></i></button>
			<ul class="categoryList">
				<li id="defaultList">
					<button id="default" class="roundBlueBt" value="default">
						<i class="fas fa-map-marker-alt">카테고리 없음</i>
					</button>
				</li>
				<li id="location" class="location"><button class="roundBlueBt" id="lbt" onclick="lbtclick()">장소</button>
				<ul class="locationList">
					<li id="location-hospital" class="location">
						<button class="smallCatBt" value="location-hospital">
							<i class="fas fa-ambulance"><br>병원</i>
						</button>
					</li>
					<li id="location-building" class="location">
						<button class="smallCatBt" value="location-building">
							<i class="fas fa-building"><br>건물</i>
						</button>
					</li>
					<li id="location-conv" class="location">
						<button class="smallCatBt" value="location-conv">
							<i class="fas fa-store-alt"><br>편의점</i>
						</button>
					</li>
					<li id="location-sleep" class="location">
						<button class="smallCatBt" value="location-sleep">
							<i class="fas fa-bed"><br>숙박</i>
						</button>
					</li>
					<li id="location-store" class="location">
						<button class="smallCatBt" value="location-store">
							<i class="fas fa-store"><br>가게</i>
						</button>
					</li>
				</ul>
				</li>
				<li id="public" class="public"><button class="roundBlueBt" id="pbt">공용</button>
				<ul class="publicList">
					<li id="public-restroom" class="public">
						<button class="smallCatBt" value="public-restroom">
							<i class="fas fa-restroom"><br>화장실</i>
						</button>
					</li>
					<li id="public-parking" class="public">
						<button class="smallCatBt" value="public-parking">
							<i class="fas fa-parking"><br>주차장</i>
						</button>
					</li>
					<li id="public-smoking" class="public">
						<button class="smallCatBt" value="public-smoking">
							<i class="fas fa-smoking"><br>흡연실</i>
						</button>
					</li>
				</ul>
				</li>
				<li id="food" class="food"><button class="roundBlueBt" id="fbt">음식점</button>
					<ul class="foodList">
						<li id="food-bar">
							<button class="smallCatBt" value="food-bar">
								<i class="fas fa-glass-martini-alt"><br>주점,바</i>
							</button>
						</li>
						<li id="food-drink">
							<button class="smallCatBt" value="food-drink">
								<i class="fas fa-beer"><br>술집</i>
							</button>
						</li>
						<li id="food-road">
							<button class="smallCatBt" value="food-road">
								<i class="fas fa-hotdog"><br>길거리</i>
							</button>
						</li>
						<li id="food-snack">
							<button class="smallCatBt" value="food-snack">
								<i class="fas fa-bread-slice"><br>간식</i>
							</button>
						</li>
						<li id="food-common">
							<button class="smallCatBt" value="food-common">
								<i class="fas fa-utensils"><br>음식점</i>
							</button>
						</li>
					</ul>
				</li>
				<li class="ent" id="ent"><button class="roundBlueBt" id="ebt">엔터</button>
					<ul class="entertainList">
						<li id="ent-book">
							<button class="smallCatBt" value="ent-book">
								<i class="fas fa-book-open"><br>서점</i>
							</button>
						</li>
						<li id="ent-sing">
							<button class="smallCatBt" value="ent-sing">
								<i class="fas fa-microphone-alt"><br>노래방</i>
							</button>
						</li>
						<li id="ent-pc">
							<button class="smallCatBt" value="ent-pc">
								<i class="fas fa-desktop"><br>pc방</i>
							</button>
						</li>
						<li id="ent-shopping">
							<button class="smallCatBt" value="ent-shopping">
								<i class="fab fa-shopify"><br>쇼핑</i>
							</button>
						</li>
					</ul>
				</li>
				<li class="sports" id="sports"><button class="roundBlueBt" id="spbt">운동</button>
					<ul class="sportList">
						<li id="sports-health">
							<button class="smallCatBt"  value="sports-health">
								<i class="fas fa-dumbbell"><br>헬스장</i>
							</button>
						</li>
						<li id="sports-soccer">
							<button class="smallCatBt" value="sports-soccer">
								<i class="fas fa-futbol"><br>축구장</i>
							</button>
						</li>
						<li id="sports-billiards">
							<button class="smallCatBt" value="sports-billiards">
								<i class="icon-snooker"><br>당구장</i>
							</button>
						</li>
						<li id="sports-bowling">
							<button class="smallCatBt" value="sports-bowling">
								<i class="fas fa-bowling-ball"><br>볼링장</i>
							</button>
						</li>
						<li id="sports-basketball">
							<button class="smallCatBt" value="sports-basketball">
								<i class="fas fa-basketball-ball"><br>농구장</i>
							</button>
						</li>
					</ul>
				</li>
			</ul>
			<HR>
			<textarea class="form-control" id="basic-url"
				aria-describedby="basic-addon3" placeholder="설명"
				style="resize: none;"></textarea>
			<HR>
			<button id="tButton" class="btn btn-primary">저장</button>
		</div>
	</div>
	<script>
		function LoadImg(value){
			if(value.files && value.files[0]){
				var reader = new FileReader();
				reader.onload = function(e){
					$('#LoadImg').attr('src',e.target.result);
				}
				reader.readAsDataURL(value.files[0]);
			}
		}
		$('#catBt').on('click',function(){
			$('.categoryList').slideDown(500);
			$('#catBt').css('display','none');
			$('#catBtup').css('display','inline-block');
		})
		$('#catBtup').on('click',function(){
			$('.categoryList').slideUp(500,function(){
				$('.publicList').slideUp(500);
				$('.foodList').slideUp(500);
				$('.entertainList').slideUp(500);
				$('.sportList').slideUp(500);
			});
			$('#catBtup').css('display','none');
			$('#catBt').css('display','inline-block');
		})
		$('#pbt').on('click',function(){
			$('.publicList').slideToggle(500);
		})
		$('#fbt').on('click',function(){
			$('.foodList').slideToggle(500);
		})
		$('#ebt').on('click',function(){
			$('.entertainList').slideToggle(500);
		})
		$('#spbt').on('click',function(){
			$('.sportList').slideToggle(500);
		})
		
		$('.smallCatBt').on('click',function(){
			if($('.smallCatBt.active')){
				$('.smallCatBt.active').removeClass('active');
			}
			$(this).toggleClass('active');
			console.log($(this).val());
		})
		$('#default').on('click',function(){
			$('.smallCatBt.active').removeClass('active');
			console.log($(this).val());
		})
		function locationclick(){
			$('.locationList').slideToggle(500);
		}
	</script>
</body>
</html>