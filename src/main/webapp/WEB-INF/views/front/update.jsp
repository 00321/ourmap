<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>OurMap - 상세정보 - 수정</title>
<!-- jQuery -->
<script src="https://code.jquery.com/jquery-3.4.1.min.js"
	integrity="sha256-CSXorXvZcTkaix6Yvo6HppcZGetbYMGWSFlBw8HfCJo="
	crossorigin="anonymous"></script>
<!-- jQuery END-->
<link rel="stylesheet" href="/css/bootstrap.min.css">
<script src="/js/bootstrap.js"></script>
<script src="/js/bootstrap.min.js"></script>
<!-- sidebar js, css -->
<link
	href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700,800,900"
	rel="stylesheet">
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
<script
	src="https://ajax.cloudflare.com/cdn-cgi/scripts/7089c43e/cloudflare-static/rocket-loader.min.js"
	data-cf-settings="2e8105826a4af43d8e85ace3-|49" defer="">
</script>
<!-- sidebar js,css end -->

<!-- fontawsome -->
<script src="https://kit.fontawesome.com/86fb79723e.js"
	crossorigin="anonymous"></script>
<!-- fontawsome end -->

<!-- mainmapcss -->
<link rel="stylesheet" href="/css/privatemapCss.css">
<!-- mainmapcss end -->

<!-- input -->
<link rel="stylesheet" href="/css/pinInput.css">
<!-- input end-->

<!-- output -->
<link rel="stylesheet" href="/css/pinOutput.css">
<!-- output end-->

<link rel="stylesheet" href="/css/searchCss.css">
<link rel="stylesheet" href="/css/searchSECss.css">
<link rel="stylesheet" href="/icomoon/style.css">
<style>
body {
	background-color: #FFFFFF;
	color: rgba(0, 0, 0, .8);
}

.container {
	margin: 5% 5% 5% 5%;
	width: 100%;
	height: 100%;
}

#header {
	width: 100%;
	height: 100%;
}

#content {
	clear: both;
	min-height: 10vh;
	border-top: 1px solid white;
	border-bottom: 1px solid white;
}

#userImg {
	color: white;
}

#profileImg {
	margin: 3% 3% 3% 3%;
	float: left;
	text-align: center;
	width: 90%;
}

#infomation {
	margin: 0 3% 3% 3%;
	width: 100%;
}

#infoUl {
	list-style: none;
	padding: 0;
}

HR {
	background-color: white;
}

li {
	margin: 2% 0 2% 0;
}

#updateBt {
	float: right;
}

#deleteBt {
	float: right;
	margin-left: 10px;
}

#closeBt {
	float: right;
	margin-left: 10px;
}

label {
	margin: 0 2% 0 0;
}

#resign {
	float: right;
}

#backcol {
	background-color: #343A40;
}

#pmContent {
	width: 80%;
	margin: 1% 3% 1% 3%;
	background-color: transparent;
	resize: none;
	border: 0;
	color: black;
}

#pmRoadad {
	width: 80%;
	margin: 1% 3% 1% 3%;
	background-color: transparent;
	resize: none;
	border: 0;
	color: black;
}

#pmContent:focus {
	outline: none;
}

#popupFooter {
	margin: 1% 3% 1% 3%;
}

#writeCMT {
	border: 1px solid white;
	border-radius: 5px;
	background-color: white;
}

#commentTA {
	background-color: transparent;
	resize: none;
	border: 0;
	color: white;
	border-bottom: 1px solid gray;
	color: black;
}

#star {
	width: 80%;
	flex: 1;
}

#star i {
	color: black;
	margin: 2% 0 1% 1%;
}

#starandsubmit {
	display: flex;
}

#cmtsubmit {
	width: 20%;
}

#cmtsubmit button {
	width: 100%;
}
</style>
</head>
<body>
	<div class="container">
		<div class="overlayboxinput">
			<div class="placeName">
				<label id="whiteCol"> 제목 : </label> <input type="hidden"
					class="form-control" id="pmnum" aria-describedby="basic-addon3">
				<input type="hidden" class="form-control" id="munum"
					aria-describedby="basic-addon3"> <input type="text"
					class="form-control" id="title" aria-describedby="basic-addon3">
			</div>
			<HR>
			<div class="first">
				<div class="center-block">
					<div class="triangle text">
						이미지 :
						<div class="filebox">
							<img id="LoadImg" width="277px" height="150px" /> <input
								type="file" id="imgAttach" name="imgAttach"
								onchange="LoadImg(this);"> <span>이미지 수정</span>
						</div>
					</div>
				</div>
			</div>
			<HR>
			<button id="catBt" onclick="catBtclick()">
				카테고리 <i class="fas fa-chevron-down"></i>
			</button>
			<button id="catBtup" onclick="catBtupclick()" style="display: none">
				카테고리 <i class="fas fa-chevron-up"></i>
			</button>
			<ul class="categoryList">
				<li id="defaultList">
					<button id="default" class="roundBlueBt" value="default"
						onclick="defaultclick(this)">
						<i class="fas fa-map-marker-alt"> 카테고리 없음</i>
					</button>
				</li>
				<li id="location" class="location"><button class="roundBlueBt"
						id="lbt" onclick="lbtclick()">장소</button>
					<ul class="locationList" id="locationID">
						<li id="location-hospital" class="location">
							<button class="smallCatBt" value="location-hospital"
								onclick="smallCatBtclick(this)">
								<i class="fas fa-ambulance"><br>병원</i>
							</button>
						</li>
						<li id="location-building" class="location">
							<button class="smallCatBt" value="location-building"
								onclick="smallCatBtclick(this)">
								<i class="fas fa-building"><br>건물</i>
							</button>
						</li>
						<li id="location-conv" class="location">
							<button class="smallCatBt" value="location-conv"
								onclick="smallCatBtclick(this)">
								<i class="fas fa-store-alt"><br>편의점</i>
							</button>
						</li>
						<li id="location-sleep" class="location">
							<button class="smallCatBt" value="location-sleep"
								onclick="smallCatBtclick(this)">
								<i class="fas fa-bed"><br>숙박</i>
							</button>
						</li>
						<li id="location-store" class="location">
							<button class="smallCatBt" value="location-store"
								onclick="smallCatBtclick(this)">
								<i class="fas fa-store"><br>가게</i>
							</button>
						</li>
					</ul></li>
				<li id="public" class="public"><button class="roundBlueBt"
						id="pbt" onclick="pbtclick()">공용</button>
					<ul class="publicList" id="publicID">
						<li id="public-restroom" class="public">
							<button class="smallCatBt" value="public-restroom"
								onclick="smallCatBtclick(this)">
								<i class="fas fa-restroom"><br>화장실</i>
							</button>
						</li>
						<li id="public-parking" class="public">
							<button class="smallCatBt" value="public-parking"
								onclick="smallCatBtclick(this)">
								<i class="fas fa-parking"><br>주차장</i>
							</button>
						</li>
						<li id="public-smoking" class="public">
							<button class="smallCatBt" value="public-smoking"
								onclick="smallCatBtclick(this)">
								<i class="fas fa-smoking"><br>흡연실</i>
							</button>
						</li>
					</ul></li>
				<li id="food" class="food"><button class="roundBlueBt" id="fbt"
						onclick="fbtclick()">음식점</button>
					<ul class="foodList" id="foodID">
						<li id="food-bar">
							<button class="smallCatBt" value="food-bar"
								onclick="smallCatBtclick(this)">
								<i class="fas fa-glass-martini-alt"><br>주점,바</i>
							</button>
						</li>
						<li id="food-drink">
							<button class="smallCatBt" value="food-drink"
								onclick="smallCatBtclick(this)">
								<i class="fas fa-beer"><br>술집</i>
							</button>
						</li>
						<li id="food-road">
							<button class="smallCatBt" value="food-road"
								onclick="smallCatBtclick(this)">
								<i class="fas fa-hotdog"><br>길거리</i>
							</button>
						</li>
						<li id="food-common">
							<button class="smallCatBt" value="food-common"
								onclick="smallCatBtclick(this)">
								<i class="fas fa-utensils"><br>음식점</i>
							</button>
						</li>
					</ul></li>
				<li class="ent" id="ent"><button class="roundBlueBt" id="ebt"
						onclick="ebtclick()">엔터</button>
					<ul class="entertainList" id="entertainID">
						<li id="ent-book">
							<button class="smallCatBt" value="ent-book"
								onclick="smallCatBtclick(this)">
								<i class="fas fa-book-open"><br>서점</i>
							</button>
						</li>
						<li id="ent-sing">
							<button class="smallCatBt" value="ent-sing"
								onclick="smallCatBtclick(this)">
								<i class="fas fa-microphone-alt"><br>노래방</i>
							</button>
						</li>
						<li id="ent-pc">
							<button class="smallCatBt" value="ent-pc"
								onclick="smallCatBtclick(this)">
								<i class="fas fa-desktop"><br>pc방</i>
							</button>
						</li>
						<li id="ent-shopping">
							<button class="smallCatBt" value="ent-shopping"
								onclick="smallCatBtclick(this)">
								<i class="fab fa-shopify"><br>쇼핑</i>
							</button>
						</li>
					</ul></li>
				<li class="sports" id="sports"><button class="roundBlueBt"
						id="spbt" onclick="spbtclick(this)">운동</button>
					<ul class="sportList" id="sportsID">
						<li id="sports-health">
							<button class="smallCatBt" value="sports-health"
								onclick="smallCatBtclick(this)">
								<i class="fas fa-dumbbell"><br>헬스장</i>'
							</button>
						</li>
						<li id="sports-soccer">
							<button class="smallCatBt" value="sports-soccer"
								onclick="smallCatBtclick(this)">
								<i class="fas fa-futbol"><br>축구장</i>'
							</button>
						</li>
						<li id="sports-billiards">
							<button class="smallCatBt" value="sports-billiards"
								onclick="smallCatBtclick(this)">
								<i class="icon-snooker"><br>당구장</i>
							</button>
						</li>
						<li id="sports-bowling">
							<button class="smallCatBt" value="sports-bowling"
								onclick="smallCatBtclick(this)">
								<i class="fas fa-bowling-ball"><br>볼링장</i>
							</button>
						</li>
						<li id="sports-basketball">
							<button class="smallCatBt" value="sports-basketball"
								onclick="smallCatBtclick(this)">
								<i class="fas fa-basketball-ball"><br>농구장</i>
							</button>
						</li>
					</ul></li>
			</ul>
			<HR>
			내용 :
			<textarea class="form-control" id="content"
				aria-describedby="basic-addon3" style="resize: none;"></textarea>
			<HR>
			<button id="tButton" class="btn btn-dark" onclick="updateMarker()">수정</button>
			<button id="tButton" class="btn btn-dark" onclick="cancel()">취소</button>
		</div>
	</div>
	<script>
		function LoadImg(value) {
			if (value.files && value.files[0]) {
				var reader = new FileReader();
				reader.onload = function(e) {
					$('#LoadImg').attr('src', e.target.result);
				}
				reader.readAsDataURL(value.files[0]);
			}
		}

		$(document).ready(
				function() {
					$.ajax({
						url : '/pmap/one?pmNum=${param.pmNum}',
						method : 'GET',
						success : function(res) {
							$('#LoadImg').attr(
									'src',
									'https://ourmapimg.s3.ap-northeast-2.amazonaws.com/img/'
											+ res.pmFile);
							$('#pmnum').val('${param.pmNum}');
							$('#munum').val(res.muNum);
							$('#title').val(res.pmTitle);
							$('#content').val(res.pmContent);
							// 카테고리 설정하지 않고 수정했을때 기존값으로 저장
							$('#category').val(res.pmCategory);
							selectedcategory = res.pmCategory;
							$('#categoryName').val(res.pmCategoryName);
							selectedcategoryname = res.pmCategoryName;
						},
						error : function(res) {
							console.log(res);
						}
					})
				})

		function updateMarker() {
			var num = document.querySelector("#pmnum");
			var img = document.querySelector("#imgAttach");
			var title = document.querySelector("#title");
			var category = selectedcategory;
			var categoryName = selectedcategoryname;
			var content = document.querySelector("#content");
			var formData = new FormData();
			formData.append('pmNum', num.value);
			formData.append('pmTitle', title.value);
			formData.append('pmContent', content.value);
			formData.append('pmCategory', category);
			formData.append('pmCategoryName', categoryName);
			if (img.files[0]) {
				formData.append('pmFile', img.files[0]);
			}
			var xhr = new XMLHttpRequest();
			xhr.open('PUT', '/pmap/update2');
			xhr.onreadystatechange = function() {
				if (xhr.readyState == xhr.DONE) {
					if (xhr.status == 200) {
						var msg = JSON.parse(xhr.responseText);
						alert(msg.msg);
						location.href = '/views/front/infoPopup?pmNum=${param.pmNum}';
						overlay = new kakao.maps.CustomOverlay({
							content : csContent,
							map : map,
							position : marker.getPosition(),
							xAnchor : 0.5,
							yAnchor : 1.25
						});
					}
				}
			}
			xhr.send(formData);
		}
		
		// 취소했을때 상세정보로 되돌아갑니다
		function cancel() {
			location.href = '/views/front/infoPopup?pmNum=${param.pmNum}';
		}

		function catBtclick() {
			$('.categoryList').slideDown(500);
			$('#catBt').css('display', 'none');
			$('#catBtup').css('display', 'inline-block');
		}
		function catBtupclick() {
			$('.categoryList')
					.slideUp(
							500,
							function() {
								var parentNode = $('button[class*=\'active\']')
										.parent().parent().attr('id');
								console.log(parentNode);
								if ($('.locationList').attr('id') == parentNode) {
									$('.publicList').slideUp(350);
									$('.foodList').slideUp(350);
									$('.entertainList').slideUp(350);
									$('.sportList').slideUp(350);
								} else if ($('.publicList').attr('id') == parentNode) {
									$('.locationList').slideUp(350);
									$('.foodList').slideUp(350);
									$('.entertainList').slideUp(350);
									$('.sportList').slideUp(350);
								} else if ($('.foodList').attr('id') == parentNode) {
									$('.locationList').slideUp(350);
									$('.publicList').slideUp(350);
									$('.entertainList').slideUp(350);
									$('.sportList').slideUp(350);
								} else if ($('.entertainList').attr('id') == parentNode) {
									$('.locationList').slideUp(350);
									$('.publicList').slideUp(350);
									$('.foodList').slideUp(350);
									$('.sportList').slideUp(350);
								} else if ($('.sportList').attr('id') == parentNode) {
									$('.locationList').slideUp(350);
									$('.publicList').slideUp(350);
									$('.foodList').slideUp(350);
									$('.entertainList').slideUp(350);
								} else {
									$('.locationList').slideUp(350);
									$('.publicList').slideUp(350);
									$('.foodList').slideUp(350);
									$('.entertainList').slideUp(350);
									$('.sportList').slideUp(350);
								}
							});
			if (!$('#default').hasClass('blackCol')) {
				$('#default').removeClass('blackCol');
				$('#default').find('i').removeClass('fas fa-check');
				$('#default').find('i').addClass('fas fa-map-marker-alt');
				$('#default').find('i').text(' 카테고리 없음');
			}
			$('#catBtup').css('display', 'none');
			$('#catBt').css('display', 'inline-block');
		}
		function lbtclick() {
			$('.locationList').slideToggle(350);
			$('.publicList').slideUp(350);
			$('.foodList').slideUp(350);
			$('.entertainList').slideUp(350);
			$('.sportList').slideUp(350);
		}
		function pbtclick() {
			$('.publicList').slideToggle(350);
			$('.locationList').slideUp(350);
			$('.foodList').slideUp(350);
			$('.entertainList').slideUp(350);
			$('.sportList').slideUp(350);
		}
		function fbtclick() {
			$('.foodList').slideToggle(350);
			$('.locationList').slideUp(350);
			$('.publicList').slideUp(350);
			$('.entertainList').slideUp(350);
			$('.sportList').slideUp(350);
		}
		function ebtclick() {
			$('.entertainList').slideToggle(350);
			$('.locationList').slideUp(350);
			$('.publicList').slideUp(350);
			$('.foodList').slideUp(350);
			$('.sportList').slideUp(350);
		}
		function spbtclick() {
			$('.sportList').slideToggle(350);
			$('.locationList').slideUp(350);
			$('.publicList').slideUp(350);
			$('.foodList').slideUp(350);
			$('.entertainList').slideUp(350);
		}

		function smallCatBtclick(value) {
			if ($('.smallCatBt.active')) {
				$('.smallCatBt.active').removeClass('active');
			}
			$(value).toggleClass('active');
			selectedcategory = $(value).val();
			selectedcategoryname = $(value).find('i').text();
			console.log($(value).val());
			console.log($(value).find('i').text());
			$('#default').removeClass('blackCol');
			$('#default').find('i').removeClass('fas fa-check');
			$('#default').find('i').addClass('fas fa-map-marker-alt');
			$('#default').find('i').text(' 카테고리 없음');
		}

		function defaultclick(value) {
			$('.smallCatBt.active').removeClass('active');
			$('#default').toggleClass('blackCol');
			selectedcategory = $(value).val();
			selectedcategoryname = '기본';
			$(value).find('i').toggleClass('fas fa-map-marker-alt');
			$(value).find('i').toggleClass('fas fa-check');
			if ($(value).find('i').text() == ' 선택됨') {
				$(value).find('i').text(' 카테고리 없음');
			} else {
				$(value).find('i').text(' 선택됨');
			}

		}
	</script>
</body>
</html>