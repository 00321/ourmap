<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Insert title here</title>
<!-- jQuery -->
<script
  src="https://code.jquery.com/jquery-3.4.1.min.js"
  integrity="sha256-CSXorXvZcTkaix6Yvo6HppcZGetbYMGWSFlBw8HfCJo="
  crossorigin="anonymous"></script> 
<!-- jQuery END-->
<link rel="stylesheet" href="/css/bootstrap.min.css">
<script src="/js/bootstrap.js"></script>
<script src="/js/bootstrap.min.js"></script>
<!-- fontawsome -->
<script src="https://kit.fontawesome.com/86fb79723e.js"
	crossorigin="anonymous"></script>
<!-- fontawsome end -->
<link rel="stylesheet" href="/css/pinOutput.css">
<style>

</style>
</head>
<body>
	<div class="container">
		<div class="overlayboxoutput">
			<div class="placeName">
				<label id="whiteCol">
					제목 들어갈 곳
				</label>
				<i class="fas fa-times fa-2x"></i> 
			</div>
			<div class="first">
				<div class="center-block">
						<div id="addr_catg_bt">
						<div class="outputfilebox">
							<img id="LoadImg" width="130px" height="100px">
							</div>
							<ul id="outputList">
								<li>주소 : <span id="pmAddr">주소들어갈곳</span></li>
								<li>카테고리 : <span id="pmCat">카테고리들어갈곳</span></li>
								<li>
									<button class="btn btn-primary">정보수정</button>
									<button class="btn btn-primary">공유</button>
								</li>
							</ul>
						</div>
				</div>
			</div>
			<div class="footer">
					<div class="footerchild"><a onclick="infopopUp()"><i class="fas fa-info"></i></a></div>
					<div class="footerchild"><a><i class="fas fa-star"></i></a></div>
					<div class="footerchild third"><a><i class="fas fa-share-alt"></i></a></div>
			</div>
		</div>
		<div class="arrow-down" > </div>
	</div>
<script>
function infopopUp(){
	window.open('/views/front/infoPopup','window','width=600','height=600');
}
</script>
</body>
</html>