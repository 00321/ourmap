<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>OurMap - 개인 지도</title>
<!-- jQuery -->
<script src="https://code.jquery.com/jquery-3.4.1.min.js"
	integrity="sha256-CSXorXvZcTkaix6Yvo6HppcZGetbYMGWSFlBw8HfCJo="
	crossorigin="anonymous"></script>
<!-- jQuery END-->
<link rel="stylesheet" href="/css/bootstrap.min.css">
<script src="/js/bootstrap.js"></script>
<script src="/js/bootstrap.min.js"></script>
<!-- sidebar js, css -->
<link
	href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700,800,900"
	rel="stylesheet">
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
<link rel="stylesheet" href="/sidebar2/css/style.css">
<script src="/sidebar2/js/bootstrap.min.js"></script>
<script src="/sidebar2/js/popper.js"
	type="2e8105826a4af43d8e85ace3-text/javascript"></script>
<script src="/sidebar2/js/main.js"
	type="2e8105826a4af43d8e85ace3-text/javascript"></script>
<script
	src="https://ajax.cloudflare.com/cdn-cgi/scripts/7089c43e/cloudflare-static/rocket-loader.min.js"
	data-cf-settings="2e8105826a4af43d8e85ace3-|49" defer="">
</script>
<!-- sidebar js,css end -->

<!-- fontawsome -->
<script src="https://kit.fontawesome.com/86fb79723e.js"
	crossorigin="anonymous"></script>
<!-- fontawsome end -->

<!-- mainmapcss -->
<link rel="stylesheet" href="/css/privatemapCss.css">
<!-- mainmapcss end -->

<!-- input -->
<link rel="stylesheet" href="/css/pinInput.css">
<!-- input end-->

<!-- output -->
<link rel="stylesheet" href="/css/pinOutput.css">
<!-- output end-->

<link rel="stylesheet" href="/css/searchCss.css">
<link rel="stylesheet" href="/css/searchSECss.css">
<link rel="stylesheet" href="/icomoon/style.css">
</head>
<body style="height: 100%">
	<button type="button" class="btn btn-primary" data-toggle="modal"
		data-target="#form" id="navLogin">login</button>
	<button type="button" class="btn btn-primary" id="navLogout"
		style="display: none">logout</button>

	<!-- modal login start -->
	<div class="modal fade" id="form" tabindex="-1" role="dialog"
		aria-labelledby="exampleModalLabel" aria-hidden="true">
		<div class="modal-dialog" role="document">
			<div class="modal-content" id="baccol">
				<div class="modal-header">
					<h5 class="modal-title" id="exampleModalLabel">로그인</h5>
					<button type="button" class="close" data-dismiss="modal"
						aria-label="Close">
						<span aria-hidden="true" style="color: white;">&times;</span>
					</button>
				</div>
				<div class="modal-body">
					<div class="form-group">
						<label for="loginId" class="col-form-label">아이디:</label> <input
							type="text" class="form-control" id="loginId">
					</div>
					<div class="form-group">
						<label for="loginPwd" class="col-form-label">비밀번호:</label> <input
							type="password" class="form-control" id="loginPwd">
						<div id="find">
							<ul>
								<li>아이디나 비밀번호가 기억나지 않으시나요?</li>
								<li><span id="findIdPwd">아이디/비밀번호 찾기</span></li>
							</ul>
						</div>
					</div>
				</div>
				<div class="modal-footer">
					<button class="btn btn-primary" data-dismiss="modal" id="loginBt">로그인</button>
					<button class="btn btn-success" id="signUp">회원가입</button>
				</div>
			</div>
		</div>
	</div>
	<!-- modal login end -->


	<div class="wrapper d-flex align-items-stretch" id="mainContent">

		<!-- sidebar start -->
		<nav id="sidebar">
			<div class="custom-menu">
				<button type="button" id="sidebarCollapse" class="btn btn-primary">
					<i class="fa fa-bars"></i> <span class="sr-only">Toggle Menu</span>
				</button>
			</div>
			<h1>
				<a href="index.html" class="logo">OurMap</a>
			</h1>
			<ul class="list-unstyled components mb-5" id="sidebarMenu">
				<li><a href="/views/front/final/publicmap" id="publicMap"><span
						class="fas fa-map-marked-alt mr-3"></span> 공유 지도</a></li>
				<li class="active"><a href="/views/front/final/privatemap"
					id="privateMap"><span class="fa fa-user mr-3"></span> 나만의 지도</a></li>
				<li><a id="searchView"><span class="fas fa-search mr-3"></span>
						검색</a></li>
				<li><a id="searchSEView"><span
						class="fas fa-map-marker-alt mr-3"></span> 경로검색</a></li>
			</ul>
			<div id="hiddenSearch" style="display: none;">
				<input type="text" id="startLo" placeholder="출발지"> <input
					type="text" id="endLo" placeholder="도착지">
				<div id="locationForSE">경로가 나오는 위치</div>
				<button class="btn btn-primary" id="goBack">돌아가기</button>
			</div>
		</nav>
		<!-- sidebar end -->

		<!-- searchbar start -->
		<nav id="searchbar" class="active">
			<div class="searchInput">
				<button class="btn btn-outline" id="sbt">
					<i class="fas fa-search"></i>
				</button>
				<input type="text" id="search" placeholder="검색어를 입력해주세요">
			</div>
			<ul class="CSList">
				<li id="pub"><span class="fas fa-users fa-1x clickEVT" id="ctg"></span>
					<span id="textSpan">공용시설</span>
					<ul class="innerList" id="innerPublic">
						<li><span class="fas fa-restroom smallCat"
							onclick="sbtClick(this)"></span>화장실</li>
						<li><span class="fas fa-parking smallCat"
							onclick="sbtClick(this)"></span>주차장</li>
						<li><span class="fas fa-smoking smallCat"
							onclick="sbtClick(this)"></span>흡연장</li>
					</ul></li>
				<li id="loc"><span
					class="fas fa-search-location fa-1x clickEVT" id="ctg"></span> <span
					id="textSpan">장소</span>
					<ul class="innerList" id="innerLocation">
						<li><span class="fas fa-ambulance smallCat"
							onclick="sbtClick(this)"></span>병원</li>
						<li><span class="fas fa-building smallCat"
							onclick="sbtClick(this)"></span>건물</li>
						<li><span class="fas fa-store-alt smallCat"
							onclick="sbtClick(this)"></span>편의점</li>
						<li><span class="fas fa-bed smallCat"
							onclick="sbtClick(this)"></span>숙박</li>
						<li><span class="fas fa-store smallCat"
							onclick="sbtClick(this)"></span>가게</li>
					</ul></li>
				<li id="foo"><span class="fas fa-utensils fa-1x clickEVT"
					id="ctg"></span> <span id="textSpan">음식</span>
					<ul class="innerList" id="innerFood">
						<li><span class="fas fa-coffee smallCat"
							onclick="sbtClick(this)"></span>카페</li>
						<li><span class="fas fa-beer smallCat"
							onclick="sbtClick(this)"></span>술집</li>
						<li><span class="fas fa-hotdog smallCat"
							onclick="sbtClick(this)"></span>길거리</li>
						<li><span class="fas fa-bread-slice smallCat"
							onclick="sbtClick(this)"></span>간식</li>
						<li><span class="fas fa-utensils smallCat"
							onclick="sbtClick(this)"></span>음식점</li>
					</ul></li>
				<li id="ent"><span class="fas fa-gamepad fa-1x clickEVT"
					id="ctg"></span> <span id="textSpan">엔터테인먼트</span>
					<ul class="innerList" id="innerEnter">
						<li><span class="fas fa-book-open smallCat"
							onclick="sbtClick(this)"></span>서점</li>
						<li><span class="fas fa-microphone-alt smallCat"
							onclick="sbtClick(this)"></span>노래방</li>
						<li><span class="fas fa-desktop smallCat"
							onclick="sbtClick(this)"></span>pc방</li>
						<li><span class="fab fa-shopify smallCat"
							onclick="sbtClick(this)"></span>쇼핑</li>
					</ul></li>
				<li id="spo"><span class="fas fa-running fa-1x clickEVT"
					id="ctg"></span> <span id="textSpan">운동</span>
					<ul class="innerList" id="innerSports">
						<li><span class="fas fa-dumbbell smallCat"
							onclick="sbtClick(this)"></span>헬스장</li>
						<li><span class="fas fa-futbol smallCat"
							onclick="sbtClick(this)"></span>축구장</li>
						<li><span class="icon-snooker smallCat"
							onclick="sbtClick(this)"></span>당구장</li>
						<li><span class="fas fa-bowling-ball smallCat"
							onclick="sbtClick(this)"></span>볼링장</li>
						<li><span class="fas fa-basketball-ball smallCat"
							onclick="sbtClick(this)"></span>농구장</li>
					</ul></li>
			</ul>
			<div id="testHidden" style="display: none;">
				<div class="result">검색결과()</div>
				<ul id="searchUL">
					<li id="searchList">
						<div class="searchImg" id="pmImg">
							사진 <img id="pmFile">
						</div>
						<div class="searchTitle" id="pmTitle">
							<p class="title">제목</p>
							<span class="searchCat" id="pmCategory">카테고리</span>
						</div>
						<div class="searchRoadAddr" id="pmRoadAd">주소</div>
						<div class="searchOldAddr" id="pmOldAd">
							지번주소
							<div class="searchInfo">
								<button class="btn btn-primary">상세보기</button>
							</div>
						</div>
					</li>
				</ul>
			</div>
			<button class="btn btn-primary categoryInit" id="returnToSidebar">
				<i class="fas fa-chevron-left"></i>
			</button>
			<button class="btn btn-primary" id="initButton">초기화</button>
		</nav>
		<!-- searchbar end -->

		<!-- SEsearchbar start-->
		<nav id="searchbarSE" class="active">
			<div class="Transportation">
				<div class="trans">
					<i class="fas fa-car"><input type="hidden" id="car" value="car"></i>
				</div>
				<div class="trans">
					<i class="fas fa-walking"><input type="hidden" id="walk"
						value="walk"></i>
				</div>
			</div>
			<div class="searchInputSE">
				<div id="totalSE">
					<div id="startlo">
						<input type="text" id="searchS" placeholder="출발지를 입력해주세요">
					</div>
					<div id="endlo">
						<input type="text" id="searchE" placeholder="도착지를 입력해주세요">
					</div>
				</div>
				<button id="sbtSE">
					<i class="fas fa-long-arrow-alt-down"></i><i
						class="fas fa-long-arrow-alt-up"></i>
				</button>
				<div id="deleteandsubmit">
					<button id="searchSELoc" onclick="fwCoords()">
						<i class="fas fa-search-location"></i>
					</button>
					<button id="delete">
						<i class="fas fa-times"></i>
					</button>
				</div>
			</div>
			<div id="searchListOfs"></div>
			<button class="btn btn-primary" id="returnToSidebarSE">
				<i class="fas fa-chevron-left"></i>
			</button>
		</nav>
		<!-- SEsearchbar end-->

		<!-- map start -->
		<div id="content">
			<div id="map" style="width: 100%; height: 100%;"></div>

			<script type="text/javascript"
				src="//dapi.kakao.com/v2/maps/sdk.js?appkey=a3fdc8c4b9ff7aa04c7acecbe9c1b0d9&libraries=services"></script>
			<script>
var selectedcategory = 'etc'; // 미설정 카테고리
var selectedcategoryname = '없음';
var mapContainer = document.getElementById('map'), // 지도를 표시할 div 
mapOption = {
	center : new kakao.maps.LatLng(33.450701, 126.570667), // 지도의 중심좌표
	level : 3, // 지도의 확대 레벨
	disableDoubleClickZoom : true
};

var geocoder = new kakao.maps.services.Geocoder(); // 주소-좌표간 변환 서비스 객체를 생성합니다

var map = new kakao.maps.Map(mapContainer, mapOption); // 지도를 생성합니다

// 마커를 표시할 위치입니다 
var position = new kakao.maps.LatLng(33.450701, 126.570667);

// 마커를 생성합니다
var marker = new kakao.maps.Marker({
	position : position,
	clickable : true
// 마커를 클릭했을 때 지도의 클릭 이벤트가 발생하지 않도록 설정합니다
});

// 마커를 지도에 표시합니다.
loadPost();

function loadPost() {
	return new Promise(function (resolve,reject){
		if (!!navigator.geolocation)

			{
				navigator.geolocation.getCurrentPosition(function(position) {
					lat = position.coords.latitude;
					lng = position.coords.longitude;
					var center = new kakao.maps.LatLng(lat, lng)
					map.setCenter(center);
					marker.setPosition(center);
					marker.setMap(null);
					resolve(position.coords);
				}, errorCallback);
			} else {
				alert("이 브라우저는 Geolocation를 지원하지 않습니다");
			}
		})
}
function errorCallback(error) {
	alert(error.message);
}

// 마커를 클릭했을 때 마커 위에 표시할 인포윈도우를 생성합니다
var iwContent = '<div class="container">'
		+ '<div class="overlayboxinput">'
		+ '<div class="placeName">'
		+ '<label id="whiteCol">'
		+ '작성양식'
		+ '</label>'
		+ '<input type="text" id="inputtitle" aria-describedby="basic-addon3" placeholder="제목">'
		+ '<input type="hidden" id="inputnum" value="${mapS.muNum}">'
		+ '</div>'
		+ '<div class="first">'
		+ '<div class="center-block">'
		+ '<div class="triangle text">'
		+ '<div class="filebox">'
		+ '<img id="LoadImg" width="277px" height="150px">'
		+ '<input type="file" id="imgAttach" name="imgAttach" onchange="LoadImg(this);">'
		+ '<span>이미지 업로드</span>'
		+ '</div>'
		+ '</div>'
		+ '</div>'
		+ '</div>'
		+'<button id="catBt" onclick="catBtclick()">카테고리 <i class="fas fa-chevron-down"></i></button>'
		+'<button id="catBtup" onclick="catBtupclick()" style="display:none">카테고리 <i class="fas fa-chevron-up"></i></button>'
		+'<ul class="categoryList">'
		+'	<li id="defaultList">'
		+'		<button id="default" class="roundBlueBt" value="default" onclick="defaultclick(this)">'
		+'			<i class="fas fa-map-marker-alt"> 카테고리 없음</i>'
		+'		</button>'
		+'	</li>'
		+'	<li id="location" class="location"><button class="roundBlueBt" id="lbt" onclick="lbtclick()">장소</button>'
		+'	<ul class="locationList" id="locationID">'
		+'		<li id="location-hospital" class="location">'
		+'			<button class="smallCatBt" value="location-hospital" onclick="smallCatBtclick(this)">'
		+'				<i class="fas fa-ambulance"><br>병원</i>'
		+'			</button>'
		+'		</li>'
		+'		<li id="location-building" class="location">'
		+'			<button class="smallCatBt" value="location-building" onclick="smallCatBtclick(this)">'
		+'				<i class="fas fa-building"><br>건물</i>'
		+'			</button>'
		+'		</li>'
		+'		<li id="location-conv" class="location">'
		+'			<button class="smallCatBt" value="location-conv" onclick="smallCatBtclick(this)">'
		+'			<i class="fas fa-store-alt"><br>편의점</i>'
		+'		</button>'
		+'	</li>'
		+'	<li id="location-sleep" class="location">'
		+'		<button class="smallCatBt" value="location-sleep" onclick="smallCatBtclick(this)">'
		+'			<i class="fas fa-bed"><br>숙박</i>'
		+'		</button>'
		+'	</li>'
		+'	<li id="location-store" class="location">'
		+'		<button class="smallCatBt" value="location-store" onclick="smallCatBtclick(this)">'
		+'			<i class="fas fa-store"><br>가게</i>'
		+'		</button>'
		+'	</li>'
		+'	</ul>'
		+'	</li>'
		+'	<li id="public" class="public"><button class="roundBlueBt" id="pbt" onclick="pbtclick()">공용</button>'
		+'<ul class="publicList" id="publicID">'
		+'	<li id="public-restroom" class="public">'
		+'		<button class="smallCatBt" value="public-restroom" onclick="smallCatBtclick(this)">'
		+'			<i class="fas fa-restroom"><br>화장실</i>'
		+'		</button>'
		+'	</li>'
		+'	<li id="public-parking" class="public">'
		+'		<button class="smallCatBt" value="public-parking" onclick="smallCatBtclick(this)">'
		+'			<i class="fas fa-parking"><br>주차장</i>'
		+'		</button>'
		+'	</li>'
		+'	<li id="public-smoking" class="public">'
		+'		<button class="smallCatBt" value="public-smoking" onclick="smallCatBtclick(this)">'
		+'			<i class="fas fa-smoking"><br>흡연실</i>'
		+'		</button>'
		+'	</li>'
		+'</ul>'
		+'</li>'
		+'<li id="food" class="food"><button class="roundBlueBt" id="fbt" onclick="fbtclick()">음식점</button>'
		+'	<ul class="foodList" id="foodID">'
		+'		<li id="food-cafe">'
		+'			<button class="smallCatBt" value="food-cafe" onclick="smallCatBtclick(this)">'
		+'				<i class="fas fa-coffee"><br>카페</i>'
		+'			</button>'
		+'		</li>'
		+'		<li id="food-drink">'
		+'			<button class="smallCatBt" value="food-drink" onclick="smallCatBtclick(this)">'
		+'				<i class="fas fa-beer"><br>술집</i>'
		+'			</button>'
		+'		</li>'
		+'		<li id="food-road">'
		+'			<button class="smallCatBt" value="food-road" onclick="smallCatBtclick(this)">'
		+'				<i class="fas fa-hotdog"><br>길거리</i>'
		+'			</button>'
		+'		</li>'
		+'		<li id="food-snack">'
		+'			<button class="smallCatBt" value="food-snack" onclick="smallCatBtclick(this)">'
		+'				<i class="fas fa-bread-slice"><br>간식</i>'
		+'			</button>'
		+'		</li>'
		+'		<li id="food-common">'
		+'			<button class="smallCatBt" value="food-common" onclick="smallCatBtclick(this)">'
		+'				<i class="fas fa-utensils"><br>음식점</i>'
		+'			</button>'
		+'		</li>'
		+'	</ul>'
		+'</li>'
		+'<li class="ent" id="ent"><button class="roundBlueBt" id="ebt" onclick="ebtclick()">엔터</button>'
		+'	<ul class="entertainList" id="entertainID">'
		+'		<li id="ent-book">'
		+'			<button class="smallCatBt" value="ent-book" onclick="smallCatBtclick(this)">'
		+'				<i class="fas fa-book-open"><br>서점</i>'
		+'			</button>'
		+'		</li>'
		+'		<li id="ent-sing">'
		+'			<button class="smallCatBt" value="ent-sing" onclick="smallCatBtclick(this)">'
		+'				<i class="fas fa-microphone-alt"><br>노래방</i>'
		+'			</button>'
		+'		</li>'
		+'		<li id="ent-pc">'
		+'			<button class="smallCatBt" value="ent-pc" onclick="smallCatBtclick(this)">'
		+'				<i class="fas fa-desktop"><br>pc방</i>'
		+'			</button>'
		+'		</li>'
		+'		<li id="ent-shopping">'
		+'			<button class="smallCatBt" value="ent-shopping" onclick="smallCatBtclick(this)">'
		+'				<i class="fab fa-shopify"><br>쇼핑</i>'
		+'			</button>'
		+'		</li>'
		+'	</ul>'
		+'</li>'
		+'<li class="sports" id="sports"><button class="roundBlueBt" id="spbt" onclick="spbtclick(this)">운동</button>'
		+'	<ul class="sportList" id="sportsID">'
		+'		<li id="sports-health">'
		+'			<button class="smallCatBt"  value="sports-health" onclick="smallCatBtclick(this)">'
		+'				<i class="fas fa-dumbbell"><br>헬스장</i>'
		+'			</button>'
		+'		</li>'
		+'		<li id="sports-soccer">'
		+'			<button class="smallCatBt" value="sports-soccer" onclick="smallCatBtclick(this)">'
		+'				<i class="fas fa-futbol"><br>축구장</i>'
		+'			</button>'
		+'		</li>'
		+'		<li id="sports-billiards">'
		+'			<button class="smallCatBt" value="sports-billiards" onclick="smallCatBtclick(this)">'
		+'				<i class="icon-snooker"><br>당구장</i>'
		+'			</button>'
		+'		</li>'
		+'		<li id="sports-bowling">'
		+'			<button class="smallCatBt" value="sports-bowling" onclick="smallCatBtclick(this)">'
		+'				<i class="fas fa-bowling-ball"><br>볼링장</i>'
		+'			</button>'
		+'		</li>'
		+'		<li id="sports-basketball">'
		+'			<button class="smallCatBt" value="sports-basketball" onclick="smallCatBtclick(this)">'
		+'				<i class="fas fa-basketball-ball"><br>농구장</i>'
		+'			</button>'
		+'		</li>'
		+'	</ul>'
		+'</li>'
		+'</ul>'
		+ '<HR>'
		+ '<textarea id="inputcontent" aria-describedby="basic-addon3" placeholder="설명"style="resize: none;"></textarea>'
		+ '<HR>'
		+ '<button id="tButton" class="btn btn-dark" onclick="saveMarker()">저장</button>'
		+ '</div>' + '</div>';
iwRemoveable = true; // removeable 속성을 ture 로 설정하면 인포윈도우를 닫을 수 있는 x버튼이 표시됩니다




// 인포윈도우를 생성합니다
var infowindow = new kakao.maps.InfoWindow({
	content : iwContent,
	removable : iwRemoveable
});

// 마커에 클릭이벤트를 등록합니다
kakao.maps.event.addListener(marker, 'click', function() {
	// 마커 위에 인포윈도우를 표시합니다
	infowindow.open(map, marker);
});

var roadad = ''; // 도로명주소 변수 전역설정
var oldad = ''; // 지번주소 변수 전역설정

//지도에 더블클릭 이벤트를 등록합니다
//지도를 더블클릭하면 마지막 파라미터로 넘어온 함수를 호출합니다
kakao.maps.event.addListener(map,'dblclick',function(mouseEvent) {
					marker.setMap(map);
					// 클릭한 위도, 경도 정보를 가져옵니다 
					var latlng = mouseEvent.latLng;
					console.log(latlng);

					// 마커 위치를 클릭한 위치로 옮깁니다
					marker.setPosition(latlng);
					latlngtest = latlng;

					infowindow.close(); // 지도를 더블클릭하면 켜져있던 인포윈도우가 닫힙니다

					searchDetailAddrFromCoords(mouseEvent.latLng, function(result, status) {
								if (status === kakao.maps.services.Status.OK) {
									roadad = !!result[0].road_address ? result[0].road_address.address_name	: '';
									oldad = result[0].address.address_name;
									// 마커를 클릭한 위치에 표시합니다 
									marker.setPosition(mouseEvent.latLng);
									marker.setMap(map);
								}
							});
					var resultDiv = document.getElementById('clickLatlng');
				});
function searchAddrFromCoords(coords, callback) {
	// 좌표로 행정동 주소 정보를 요청합니다
	var addrinfo = geocoder.coord2RegionCode(coords.getLng(), coords
			.getLat(), callback);
}

function searchDetailAddrFromCoords(coords, callback) {
	// 좌표로  상세 주소 정보를 요청합니다
	var addrinfoinfo = geocoder.coord2Address(coords.getLng(), coords
			.getLat(), callback);
}
var usernumberis = '${mapS.muNum}';

// 장소저장 function입니다
function saveMarker() {
	if(usernumberis != ''){
		var userNum = usernumberis;
	}
	var img = document.querySelector("#imgAttach");
	var title = document.querySelector("#inputtitle");
	var category = selectedcategory;
	var categoryname = selectedcategoryname;
	var content = document.querySelector("#inputcontent");
	
	var formData = new FormData();
	formData.append('muNum',userNum);
	formData.append('pmTitle', title.value);
	formData.append('pmContent', content.value);
	formData.append('pmCategory', category);
	formData.append('pmCategoryName', categoryname);
	formData.append('pmRoadad', roadad);
	formData.append('pmOldad', oldad);
	if (img.files[0]) {
		formData.append('pmFileItem', img.files[0]);
	}
	formData.append('pmLatitude', latlngtest.Ha);
	formData.append('pmLongitude', latlngtest.Ga);
	console.log(formData);
	console.log(img.files[0]);

	var xhr = new XMLHttpRequest();
	xhr.open('POST', '/pmap/insert');
	xhr.onreadystatechange = function() {
		if (xhr.readyState == xhr.DONE) {
			if (xhr.status == 200) {
				console.log(xhr.responseText);
				var msg = JSON.parse(xhr.responseText);
				alert(msg.msg);
				location.href = '/views/front/final/privatemap';
				infowindow.close();
				infowindow = new kakao.maps.InfoWindow({
					content : iwContent,
					removable : iwRemoveable
				});
			}
		}
	}
	xhr.send(formData);
}

var clickedOverlay = '';
var markers = [];
var markerImage;
var myNum = '${mapS.muNum}';

window.onload = function() {
	if(myNum == ''){
		alert('로그인이 안되있네요 퍼블릭맵으로 가시오');
		location.href='/views/front/final/publicmap';
	}
	
	var xhr = new XMLHttpRequest();
	xhr.open('POST', '/pmap/list');
	xhr.setRequestHeader('Content-type','application/json;utf-8');
	xhr.onreadystatechange = function() {
		if (xhr.readyState == xhr.DONE) {
			if (xhr.status == 200) {
				function makeOverListener(map, marker, infowindow) {
					return function() {
						infowindow.open(map, marker);
					};
				}
				function makeOutListener(infowindow) {
					return function() {
						infowindow.close();
					};
				}
				var privateMapList = JSON.parse(xhr.responseText);
				for (var i = 0; i < privateMapList.length; i++) {
					if(privateMapList[i].pmRoadad==''){
						privateMapList[i].pmRoadad=privateMapList[i].pmOldad
					}
					var pmLat = privateMapList[i].pmLatitude;
					var pmLng = privateMapList[i].pmLongitude;
					var position = new kakao.maps.LatLng(pmLat, pmLng);
					
					var imageSrc = 'https://ourmapimg.s3.ap-northeast-2.amazonaws.com/marker/'+privateMapList[i].pmCategory+'.png', // 마커이미지의 주소입니다    
					imageSize = new kakao.maps.Size(35, 30), // 마커이미지의 크기입니다
					imageOption = {offset: new kakao.maps.Point(15,20)}; // 마커이미지의 옵션입니다. 마커의 좌표와 일치시킬 이미지 안에서의 좌표를 설정합니다.
					markerImage = new kakao.maps.MarkerImage(imageSrc, imageSize, imageOption);
					var marker = new kakao.maps.Marker({
						position : position,
						image : markerImage
					});
					markers.push(marker);
					markers[i].setMap(map);

					var csContent = '';
					var overlayboxoutput = '';
					var placeName = '';
					var whiteCol = '';
					var closeAncor = '';
					var first = '';
					var centerblock = '';
					var addrcatgbt = '';
					var outputfilebox = '';
					var footer = '';
					var footerchild = '';
					var footerchildtwo = '';
					var footerchildthird = '';
					var arrowdown = '';
					var xicon = '';

					privateMapList.forEach(function(element) {
								var overlay = new kakao.maps.CustomOverlay({
											map : map,
											position : marker.getPosition(),
											xAnchor : 0.5,
											yAnchor : 1.17,
											clickable : true
										});

								csContent = document.createElement("div");
								csContent.className = 'container'; 
								
								overlayboxoutput = document.createElement("div");
								overlayboxoutput.className = 'overlayboxoutput';
								

								placeName = document.createElement("div");
								placeName.className = 'placeName';
								

								whiteCol = document.createElement('label');
								whiteCol.id = 'whiteCol';
								whiteCol.innerHTML = privateMapList[i].pmTitle
										+ '<input type="hidden" class="form-control" id="pmnum"	aria-describedby="basic-addon3" value=' + privateMapList[i].pmNum + '>'
										+ '<input type="hidden" class="form-control" id="munum"	aria-describedby="basic-addon3" value=' + privateMapList[i].muNum + '>'

								closeAncor = document.createElement('a');
								closeAncor.id = 'close';
								xicon = document.createElement('i');
								xicon.className = 'fas fa-times fa-2x';
								closeAncor.appendChild(xicon);
								closeAncor.onclick = function() {
									clickedOverlay.setMap(null);
								}

								first = document.createElement("div");
								first.className = 'first';
								

								centerblock = document.createElement("div");
								centerblock.className = 'center-block';
								

								addrcatgbt = document.createElement("div");
								addrcatgbt.id = 'addr_catg_bt';
								addrcatgbt.innerHTML = '<div class="outputfilebox">'
										+ '<img id="LoadImgOutput" width="130px" height="100px" src="https://ourmapimg.s3.ap-northeast-2.amazonaws.com/img/'+privateMapList[i].pmFile+'">'
										+	'</div>'
										+	'<ul id="outputList">'
										+ '<li>주소 : <span id="pmAddr">'+privateMapList[i].pmRoadad
										+'</span></li>'
										+ '<li>카테고리 : <span id="pmCategoryName">'
										+ privateMapList[i].pmCategoryName
										+ '</span></li>'
										+ '<li>'
										+ '<button class="btn btn-primary" onclick="savePublic('+privateMapList[i].pmNum+')">공유</button>'
										+ '<button class="btn btn-primary" onclick="insertAddress(this)">출발</button>'
										+ '<button class="btn btn-primary" onclick="insertAddress(this)">도착</button>'
										+ '</li>' 
										+ '</ul>';
								footer = document.createElement("div");
								footer.className = 'footer';
								

								footerchild = document.createElement("div");
								footerchild.className = 'footerchild';
								footerchild.innerHTML = '<a id="mkInfo" onclick="infopopUp('
										+ privateMapList[i].pmNum
										+ ')"><i class="fas fa-info"></i></a>';

								footerchildtwo = document.createElement("div");
								footerchildtwo.className = 'footerchild';
								footerchildtwo.innerHTML = '<a><i class="fas fa-star"></i></a>';

								footerchildthird = document.createElement("div");
								footerchildthird.className = 'footerchild third';
								footerchildthird.innerHTML = '<a><i class="fas fa-share-alt"></i></a>';

								arrowdown = document.createElement("div");
								arrowdown.className = 'arrow-down';
								
								footer.append(footerchild);
								footer.append(footerchildtwo);
								footer.append(footerchildthird);
								
								
								centerblock.append(addrcatgbt);
								
								first.append(centerblock);
								
								placeName.append(whiteCol);
								placeName.append(closeAncor);
								
								overlayboxoutput.append(placeName);
								overlayboxoutput.append(first);
								overlayboxoutput.append(footer);
								
								csContent.append(overlayboxoutput);
								csContent.append(arrowdown);
								
								overlay.setContent(csContent);

								overlay.setMap(null);
								clickedOverlay = overlay;
								kakao.maps.event.addListener(marker,'click', function() {
											if (clickedOverlay) {
												clickedOverlay.setMap(null);
											}
											overlay.setMap(map);
											clickedOverlay = overlay;
										});

								function closeOverlay() {
									clickedOverlay.setMap(null);
								}

							})
				}
				
			}
		}
	}
	var unumber = {
			muNum : myNum
	}
	var json = JSON.stringify(unumber);
	xhr.send(json);
}

kakao.maps.event.addListener(map, 'click', function(mouseEvent) {
	if(clickedOverlay){
		clickedOverlay.setMap(null);
	}
	infowindow.close();
});

// 이미지 불러오기 function입니다
function LoadImg(value) {
	if (value.files && value.files[0]) {
		var reader = new FileReader();
		reader.onload = function(e) {
			$('#LoadImg').attr('src', e.target.result);
		}
		reader.readAsDataURL(value.files[0]);
	}
}

// 상세정보 function입니다
function infopopUp(f) {
	console.log(f);
	window.open('/views/front/infoPopup?pmNum=' + f, 'window',
			'width=600', 'height=600');
}

// 카테고리 버튼 function입니다
function catBtclick(){
	$('.categoryList').slideDown(500);
	$('#catBt').css('display','none');
	$('#catBtup').css('display','inline-block');
}
function catBtupclick(){
	$('.categoryList').slideUp(500,function(){
		var parentNode = $('button[class*=\'active\']').parent().parent().attr('id');
		console.log(parentNode);
		if($('.locationList').attr('id') == parentNode){
			$('.publicList').slideUp(350);
			$('.foodList').slideUp(350);
			$('.entertainList').slideUp(350);
			$('.sportList').slideUp(350);
		}
		else if($('.publicList').attr('id') == parentNode){
			$('.locationList').slideUp(350);
			$('.foodList').slideUp(350);
			$('.entertainList').slideUp(350);
			$('.sportList').slideUp(350);
		}
		else if($('.foodList').attr('id') == parentNode){
			$('.locationList').slideUp(350);
			$('.publicList').slideUp(350);
			$('.entertainList').slideUp(350);
			$('.sportList').slideUp(350);
		}
		else if($('.entertainList').attr('id') == parentNode){
			$('.locationList').slideUp(350);
			$('.publicList').slideUp(350);
			$('.foodList').slideUp(350);
			$('.sportList').slideUp(350);
		}
		else if($('.sportList').attr('id') == parentNode){
			$('.locationList').slideUp(350);
			$('.publicList').slideUp(350);
			$('.foodList').slideUp(350);
			$('.entertainList').slideUp(350);
		}
		else{
			$('.locationList').slideUp(350);
			$('.publicList').slideUp(350);
			$('.foodList').slideUp(350);
			$('.entertainList').slideUp(350);
			$('.sportList').slideUp(350);
		}
	});
	if(!$('#default').hasClass('blackCol')){
		$('#default').removeClass('blackCol');
		$('#default').find('i').removeClass('fas fa-check');
		$('#default').find('i').addClass('fas fa-map-marker-alt');
		$('#default').find('i').text(' 카테고리 없음');
	}
	$('#catBtup').css('display','none');
	$('#catBt').css('display','inline-block');
}
function lbtclick(){
	$('.locationList').slideToggle(350);
	$('.publicList').slideUp(350);
	$('.foodList').slideUp(350);
	$('.entertainList').slideUp(350);
	$('.sportList').slideUp(350);
}
function pbtclick(){
	$('.publicList').slideToggle(350);
	$('.locationList').slideUp(350);
	$('.foodList').slideUp(350);
	$('.entertainList').slideUp(350);
	$('.sportList').slideUp(350);
}
function fbtclick(){
	$('.foodList').slideToggle(350);
	$('.locationList').slideUp(350);
	$('.publicList').slideUp(350);
	$('.entertainList').slideUp(350);
	$('.sportList').slideUp(350);
}
function ebtclick(){
	$('.entertainList').slideToggle(350);
	$('.locationList').slideUp(350);
	$('.publicList').slideUp(350);
	$('.foodList').slideUp(350);
	$('.sportList').slideUp(350);
}
function spbtclick(){
	$('.sportList').slideToggle(350);
	$('.locationList').slideUp(350);
	$('.publicList').slideUp(350);
	$('.foodList').slideUp(350);
	$('.entertainList').slideUp(350);
}

function smallCatBtclick(value){
	if($('.smallCatBt.active')){
		$('.smallCatBt.active').removeClass('active');
	}
	$(value).toggleClass('active');
	selectedcategory = $(value).val();
	selectedcategoryname = $(value).find('i').text();
	console.log($(value).val());
	console.log($(value).find('i').text());
	$('#default').removeClass('blackCol');
	$('#default').find('i').removeClass('fas fa-check');
	$('#default').find('i').addClass('fas fa-map-marker-alt');
	$('#default').find('i').text(' 카테고리 없음');
}

function defaultclick(value){
	$('.smallCatBt.active').removeClass('active');
	$('#default').toggleClass('blackCol');
	selectedcategory = 'etc';
	selectedcategoryname = '없음';
	$(value).find('i').toggleClass('fas fa-map-marker-alt');
	$(value).find('i').toggleClass('fas fa-check');
	if($(value).find('i').text() == ' 선택됨'){
		$(value).find('i').text(' 카테고리 없음');
	} else{
		$(value).find('i').text(' 선택됨');
	}
	$('.locationList').slideUp(350);
	$('.publicList').slideUp(350);
	$('.foodList').slideUp(350);
	$('.entertainList').slideUp(350);
	$('.sportList').slideUp(350);
}

$('#signUp').on('click',function(){
	location.href = '/views/front/user/signup';
}) 
var shareList;

// 공유 function입니다
function savePublic(f) {
	$.ajax({
		url : '/pmap/one2?pmNum='+f,
		method : 'GET',
		success : function(res) {
			console.log(res);
			shareList = res;
			$('#pmnum').val(res.pmNum);
			$('#munum').val(res.muNum);
			$('#title').val(res.pmTitle);
			$('#content').val(res.pmContent);
			$('#category').val(res.pmCategory);
			$('#categoryName').val(res.pmCategoryName);
			shareToPublicMap();
		},
		error : function(res) {
			console.log(res);
		}
	})
	
}

// 공유확인 function입니다
function shareToPublicMap(){
	var con = confirm("공유하시겠습니까?");
	if(con) {
	var img = $('#LoadImgOutput').attr('src');
	var formData = new FormData();
	formData.append('muNum',usernumberis);
	formData.append('pmNum',shareList.pmNum)
	formData.append('bmTitle', shareList.pmTitle);
	formData.append('bmContent', shareList.pmContent);
	formData.append('bmCategory', shareList.pmCategory);
	formData.append('bmCategoryName', shareList.pmCategoryName);
	formData.append('bmRoadad', shareList.pmRoadad);
	formData.append('bmOldad', shareList.pmOldad);
	formData.append('bmFile', shareList.pmFile);
	formData.append('bmLatitude', shareList.pmLatitude);
	formData.append('bmLongitude', shareList.pmLongitude);
	console.log(formData);

	var xhr = new XMLHttpRequest();
	xhr.open('POST', '/bmap/insert');
	xhr.onreadystatechange = function() {
		if (xhr.readyState == xhr.DONE) {
			if (xhr.status == 200) {
				console.log(xhr.responseText);
				var msg = JSON.parse(xhr.responseText);
				alert(msg.msg);
				location.href = '/views/front/final/privatemap';
				infowindow.close();
				infowindow = new kakao.maps.InfoWindow({
					content : iwContent,
					removable : iwRemoveable
				});
			}
		}
	}
	xhr.send(formData);
	}
}

</script>
			<!-- map end -->

		</div>
	</div>
	<script>
var searchMarkers = [];
var saveSinfoWindow;
$(document).ready(function(){
	$('#pinMenuR').on('click',function(){
		toggleLeft(this);
	});
	$('#pinMenuL').on('click',function(){
		$('#utilListDiv').addClass('active');
		toggleRight(this);
	});
	
	$('#searchView').on('click',function(){
		$('#sidebar').toggleClass('active');
		$('#searchbar').toggleClass('active');
	})
	
	$('#searchSEView').on('click',function(){
		$('#sidebar').toggleClass('active');
		$('#searchbarSE').toggleClass('active');
	})
	
	$('#utilBt').on('click',function(){
		$('#utilListDiv').toggleClass('active');
	})
	
	$('#goBack').on('click',function(){
		$('#sidebarMenu').css('display','block');
		$('#hiddenSearch').css('display','none');
	})
	
	$('#loginBt').on('click',function(){
		var login = {
				muId:$('#loginId').val(),
				muPwd:$('#loginPwd').val()
			}
		$.ajax({
			url:'/mapuser/login',
			method:'POST',
			data:login,
			success:function(res){
				if(res.result == 1){
					alert('로그인 성공');
					console.log(res);
					location.href='/views/front/final/publicmap';
				} else{
					alert('로그인 실패. 아이디와 비밀번호를 다시 확인해주세요');
					console.log(res);
				}
			}
		})
	})
	
	if('${mapS.muName}' != ''){
		$('#navLogin').css('display','none');
		$('#navLogout').css('display','block'); 
		$('#sidebarMenu').append('<li><a id="userinfo" onclick="popUp()"><span class="fa fa-paper-plane mr-3" ></span>내정보</a></li>')
		$('#navLogout').on('click',function(){
			$.ajax({
				url:'/mapuser/logout',
				method:'POST',
				success:function(res){
					if(res.msg){
						alert(res.msg);
						$('#navLogin').css('display','block');
						$('#navLogout').css('display','none');

						location.href='/views/front/final/publicmap';
					} else{
						alert('오류');
					}
				},
				error:function(res){
					console.log(res);
				}
			})
		})
	}
	
	$('#initButton').on('click',function(){
		$('.innerList').slideUp(0);
		$('.clickEVT.active').removeClass('active');
		$('.CSList').css('display','block');
		$('#testHidden').css('display','none');
		for(var i = 0; i<searchMarkers.length;i++){
			searchMarkers[i].setMap(null);
		}
		if(saveSinfoWindow){
			saveSinfoWindow.close();
		}
	})
	
})

function toggleLeft(f){
		$('#pinMenuR').css('display','none');
		$('#pinMenuL').css('display','inline');
		
		var divWidth = $('#hiddenMenu').width();
		
		$('#hiddenMenu').css('display','inline');
		
		$("#toggleUl").animate({
				width: "+=450px"
			  },"slow");
}

function toggleRight(){
	$('#pinMenuR').css('display','inline');
	$('#pinMenuL').css('display','none');
		$("#toggleUl").animate({
			width: "-=450px"
		  },"slow");
	}

// 사용자정보 function입니다
function popUp(){
	window.open('/views/front/user/UserInfo','window','width=350','height=400');
	var strWidth;
	var strHeight;
	 
	  //innerWidth / innerHeight / outerWidth / outerHeight 지원 브라우저 
	  if ( window.innerWidth && window.innerHeight && window.outerWidth && window.outerHeight ) {
	    strWidth = $('#userinfo').outerWidth() + (window.outerWidth - window.innerWidth);
	    strHeight = $('#userinfo').outerHeight() + (window.outerHeight - window.innerHeight);
	  }
	  else {
	    var strDocumentWidth = $(document).outerWidth();
	    var strDocumentHeight = $(document).outerHeight();
	 
	    window.resizeTo ( strDocumentWidth, strDocumentHeight );
	    
	    var strMenuWidth = strDocumentWidth - $(window).width();
	    var strMenuHeight = strDocumentHeight - $(window).height();
	
	    strWidth = $('#container').outerWidth() + strMenuWidth;
	    strHeight = $('#container').outerHeight() + strMenuHeight;
	  }
	  //resize 
	  window.resizeTo( strWidth, strHeight );

}

$('#findIdPwd').on('click',function(){
	location.href='/views/front/user/findIdPwd';
})


$('#sidebarMenu > li').on('click',function(){
	if($('#sidebarMenu > li.active')){
		$('#sidebarMenu > li.active').toggleClass('active');
	}
	$(this).toggleClass('active');
})

var listOfSearch = [];
var bounds = new kakao.maps.LatLngBounds();

async function searchLocation(){
	if($("#search").val().trim() == ''){
		alert('검색어를 입력해주세요');
		return;
	}
	var tempPosition = map.getCenter();
	console.log(map.getCenter());
	var latitude = tempPosition.Ha;
	var longitude = tempPosition.Ga;
	
	for(var i=0;i<searchMarkers.length;i++){
		searchMarkers[i].setMap(null);
	}
	
	var keyword = $('#search').val();
	$.ajax({
		method : 'GET',
		url : '/keyword/search?latitude='+latitude+'&longitude='+longitude+'&keyword='+keyword,
		success : function(res){
			listOfSearch = [];
			searchMarkers = [];
			console.log(listOfSearch);
			console.log(searchMarkers);
			for(var a=0;a<res.documents.length;a++){
				listOfSearch.push(res.documents[a]);
				var markerPosition = new kakao.maps.LatLng(res.documents[a].y,res.documents[a].x);
				var searchMarker = new kakao.maps.Marker({
					map: map,
					position: markerPosition
				});
				var sminfowindow = new kakao.maps.InfoWindow({
			        content: res.documents[a].place_name
			    });
				searchMarkers.push(searchMarker);
				searchMarkers[a].setMap(map);
				
				kakao.maps.event.addListener(searchMarker, 'mouseover', makeOverListener(map, searchMarker, sminfowindow));
			    kakao.maps.event.addListener(searchMarker, 'mouseout', makeOutListener(sminfowindow));
			}
			$('.CSList').css('display','none');
			$('#testHidden').css('display','block');
			var resultList = '';
			for(var i=0;i<res.documents.length;i++){
				if(res.documents[i].road_address_name==''){
					res.documents[i].road_address_name='<br>';
				}
				resultList += '<ul id="searchUL">'
				     +'<li id="searchList">'
				     +'<div class="searchImg" id="pmImg">사진<img id="pmFile"></div>'
				     +' <div class="searchTitle" id="pmTitle" onclick="clickTitle('+i+','+res.documents[i].y+','+res.documents[i].x+')">'
				     +'<p class="title">'+res.documents[i].place_name+'</p><span class="searchCat" id="pmCategory">'+res.documents[i].category_group_name+'</span>'
				     +'</div>'
				     +' <div class="searchRoadAddr" id="pmRoadAd">도로명 : '+res.documents[i].road_address_name+'</div>'
				     +' <div class="searchOldAddr" id="pmOldAd">지번 : '+res.documents[i].address_name
				     +' <br>'
				     +'<div class="searchInfo"><button class="btn btn-primary" onclick="goPage(\''+res.documents[i].place_url+'\')">상세보기</button></div>'
				     +' </div>'
				     +' </li>' 
				     +'</ul>'
			}
			$('#testHidden').html(resultList);
			
			if($('#testHidden').height() > '650'){
				$('#testHidden').height('650');
				$('#testHidden').addClass('overSearch');
			}
			setCenterToSearchLocation(res.documents[0].y,res.documents[0].x);
		}
	})
}

function clickTitle(i,lat,lng){
	var iwContent = listOfSearch[i].place_name, 
    iwPosition = new kakao.maps.LatLng(lat, lng);
	
	var sinfowindow = new kakao.maps.InfoWindow({
	    position : iwPosition, 
	    content : iwContent 
	});
	if(saveSinfoWindow){
		saveSinfoWindow.close();
	}
	sinfowindow.open(map,searchMarkers[i]);
	saveSinfoWindow = sinfowindow;
}
function displayInfowindow(smarker, title) {
    var content = '<div style="padding:5px;z-index:1;">' + title + '</div>';

    infowindow.setContent(content);
    infowindow.open(map, smarker);
}

//인포윈도우를 표시하는 클로저를 만드는 함수입니다 
function makeOverListener(map, marker, sminfowindow) {
    return function() {
    	sminfowindow.open(map, marker);
    };
}

// 인포윈도우를 닫는 클로저를 만드는 함수입니다 
function makeOutListener(sminfowindow) {
    return function() {
    	sminfowindow.close();
    };
}

function setCenterToSearchLocation(x,y){
	var moveLatLon = new kakao.maps.LatLng(x, y);
	map.panTo(moveLatLon);
}

function myLocation(){
	var position = map.getCenter();
	var moveLatLon = new kakao.maps.LatLng(position.Ga, position.Ha);
	map.panTo(moveLatLon);
}

function sbtClick(value){
	$('#search').val($(value).parent().text());
	$('#sbt').trigger('click');
}

//길찾기 선 저장변수 전역설정입니다
var savepolyline;

$('#sbt').bind('click',function(){
	searchLocation();
})

$("#search").keydown(function(key) {
	if (key.keyCode == 13) {
		searchLocation();
	}
});

//사이드바 뒤로가기 버튼 클릭 이벤트입니다
$('#returnToSidebar').on('click',function(){
	$('#testHidden').css('display','none');
	$('#sidebar').removeClass('active');
	$('#searchbar').addClass('active');
	$('.CSList').css('display','inline');
	saveSinfoWindow.close();
	for(var i=0;i<searchMarkers.length;i++){
		searchMarkers[i].setMap(null);
	}
	if('btn btn-primary categoryInit' == $(this).attr('class')){
		$('.innerList').slideUp(350);
		$('#search').val(null);
	}
})

// 사이드바 뒤로가기 버튼 클릭 이벤트입니다(길찾기)
$('#returnToSidebarSE').on('click',function(){
	$('#sidebar').removeClass('active');
	$('#searchbarSE').addClass('active');
	if(savepolyline){
		savepolyline.setMap(null);
	}
})

$('.trans').on('click',function(){
	if($('.trans.active')){
		$('.trans.active').removeClass('active');
	}
	$(this).toggleClass('active');
})
$('#sbtSE').on('click',function(){
	var start = $('#searchS').val();
	var end = $('#searchE').val();
	$('#searchE').val(start);
	$('#searchS').val(end);
})

// 길찾기 초기화 버튼 클릭이벤트입니다
$('#delete').on('click',function(){
	$('#searchE').val(null);
	$('#searchS').val(null);
	if(savepolyline){
		savepolyline.setMap(null);
	}
})

$('.clickEVT').on('click',function(){
	if($('.clickEVT.active')){
		$('.clickEVT.active').parent().children('ul').slideUp(350);
		$('.clickEVT.active').removeClass('active');
	}
	$(this).toggleClass('active');
	$(this).parent().children('ul').slideDown(350);
})

$('.smallCat').on('click',function(){
	$('#search').val($(this).parent().text());
})
function goPage(value){
	window.open(value,'window','width=350','height=400');
}

function insertAddress (getButton){
	var button = getButton.innerHTML;
	var lili = getButton.parentNode;
	var lili2 = lili.previousSibling.previousSibling;
	var child = lili2.childNodes;
	var addr = child[1].innerHTML;
	if (button=='출발'){
		$('#searchS').val(addr);
	}
	else if (button=='도착'){
		$('#searchE').val(addr);
	}
	$('#sidebar').addClass('active');
	$('#searchbarSE').removeClass('active');
} 

function getCoords(abc){
	return new Promise(function(resolve,reject){
		geocoder.addressSearch(abc, function(result, status) {
	     	if (status === daum.maps.services.Status.OK) {
	    	 	var coords = result[0].road_address;
	    	 	resolve(coords);
	   		 	} 
			})
		})
}

// 길찾기 이벤트입니다
async function fwCoords(){
	if(savepolyline){
		savepolyline.setMap(null);
	}
	var trans = $('.trans');
	var mode = '';
	for (tran of trans){
		if (tran.className=='trans active'){
			mode = tran.firstChild.className;
		} 
	}
	if (mode.indexOf('walk')!=-1){
		mode = 'walk';
	}
	
	var startPoint = $('#searchS').val();
	var endPoint = $('#searchE').val();
	var coords = [];
	if (!startPoint){
		var centerPoint = await loadPost();
		coords.push(centerPoint.longitude);
		coords.push(centerPoint.latitude);
		var baba = await getCoords(endPoint);
		coords.push(baba.x);
		coords.push(baba.y);
	}else if(!endPoint){
		var centerPoint = await loadPost();
		var baba = await getCoords(startPoint);
		coords.push(baba.x);
		coords.push(baba.y);
		coords.push(centerPoint.longitude)
		coords.push(centerPoint.latitude)
	}else if (endPoint&&startPoint){
		var listData = [startPoint,endPoint];
		for (var i=0; i < listData.length ; i++) {
			var baba = await getCoords(listData[i]);
			var x = baba.x
			var y = baba.y
			coords.push(x);
			coords.push(y);
		}
	}
	
	var param = {
			startX : coords[0],
			startY : coords[1],
			endX : coords[2],
			endY : coords[3],
			mode : mode
	} 
	var xhr = new XMLHttpRequest();
	xhr.open('GET', '/tMap?startX='+param.startX+'&startY='+param.startY+'&endX='+param.endX+'&endY='+param.endY+'&mode='+param.mode);
	xhr.onreadystatechange = function() {
		if (xhr.readyState == xhr.DONE) {
			if (xhr.status == 200) {
				var allPath = JSON.parse(xhr.responseText); //라인을 그릴 좌표와 프로퍼티에 관한 정보를 전부받아옴
				var routes = allPath.coordinates;
				var getProperties = allPath.properties;
				var totalTime = getProperties.totalTime;
				var totalDistance = getProperties.totalDistance;
				if(getProperties.taxiFare){
				var taxiFare = getProperties.taxiFare;
				}
				console.log(taxiFare);
				var linePath = new Array();
				for (var route of routes){
					var pmLat = route[1];
					var pmLng = route[0];
					var position = new kakao.maps.LatLng(pmLat, pmLng);
					linePath.push(position);
				}
				
				var polyline = new kakao.maps.Polyline({
				    path: linePath, // 선을 구성하는 좌표배열 입니다
				    strokeWeight: 5, // 선의 두께 입니다
				    strokeColor: '#FF0000', // 선의 색깔입니다
				    strokeOpacity: 0.7, // 선의 불투명도 입니다 1에서 0 사이의 값이며 0에 가까울수록 투명합니다
				    strokeStyle: 'solid' // 선의 스타일입니다
				});
				savepolyline = polyline;
				// 지도에 선을 표시합니다 
				polyline.setMap(map);  
				kakao.maps.event.addListener(map, 'dblclick', function() {
					polyline.setMap(null);
				});
			}
		}
	}
	xhr.send();
}
</script>
</body>
</html>