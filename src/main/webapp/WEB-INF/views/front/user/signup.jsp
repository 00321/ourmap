<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>OurMap - 회원 가입</title>
<!-- jQuery -->
<script src="https://code.jquery.com/jquery-3.4.1.min.js"
	integrity="sha256-CSXorXvZcTkaix6Yvo6HppcZGetbYMGWSFlBw8HfCJo="
	crossorigin="anonymous"></script>
<!-- jQuery END-->

<!-- Custom fonts for this template-->
<link href="/sbadmin/vendor/fontawesome-free/css/all.min.css"
	rel="stylesheet" type="text/css">
<!-- Custom styles for this template-->
<link href="/sbadmin/css/sb-admin.css" rel="stylesheet">
<!-- Core plugin JavaScript-->
<script src="/sbadmin/vendor/jquery-easing/jquery.easing.min.js"></script>

<script src="/js/bootstrap.min.js"></script>
<script src="/js/bootstrap.bundle.js"></script>
<link rel="stylesheet" href="/css/bootstrap.min.css" />
<link rel="stylesheet" href="/css/bootstrap-reboot.min.css" />
<link rel="stylesheet" href="/css/bootstrap-grid.min.css" />
<style>
#muAge {
	width: 70px;
	margin-left: 10%;
	margin-right: 60%;
}

#agelabel {
	width: 300px;
	display: flex;
	text-align: center;
}

#emailBt {
	margin-top: 5%;
}

#inlineRadio1 {
	margin-right: 7%;
}

.form-group {
	clear: both;
}

#emailForm {
	height: 100%;
}

#checkNum {
	width: 100px;
	margin-right: 2%;
}

#hiddenInput {
	margin: 2% 2% 2% 0;
	height: 40px;
}

#submitEmail {
	width: 70px;
}

#hiddenUl {
	list-style: none;
	margin: 0;
	padding: 0;
}

#hiddenUl li {
	float: left;
	margin-right: 3%;
}

#jungbok {
	list-style: none;
	margin: 0;
	padding: 0;
}

#jungbok li {
	float: left;
	margin-right: 5%;
}
</style>
</head>
<body class="bg-dark">
	<div class="container">
		<div class="card card-login mx-auto mt-5">
			<div class="card-header">
				<h3>회원가입</h3>
			</div>
			<div class="card-body">
				<div class="form-group">
					<label for="muId" class="col-form-label">아이디 : </label>
					<ul id="jungbok">
						<li><input type="text" class="form-control" id="muId"></li>
						<li><button class="btn btn-warning" id="jbCheck">중복확인</button></li>
						<li><div id="hiddenJB"></div></li>
					</ul>
					<input type="hidden" id="jbCheckInput" value="notclicked">
				</div>
				<HR>
				<div class="form-group">
					<label for="muName" class="col-form-label">성명 : </label> <input
						type="text" class="form-control" id="muName">
				</div>
				<HR>
				<div class="form-group">
					<label for="muPwd" class="col-form-label">비밀번호 : </label> <input
						type="password" class="form-control" id="muPwd">
				</div>
				<div class="form-group">
					<label for="pwdCheck" class="col-form-label">비밀번호확인 : </label> <input
						type="password" class="form-control" id="pwdCheck">
				</div>
				<HR>
				<div class="form-check form-check-inline">
					<label for="muAge" class="col-form-label" id="agelabel">나이
						: </label> <input type="number" class="form-control" id="muAge">
				</div>
				<HR>
				<div class="form-group" id="emailForm">
					<label for="muEmail" class="col-form-label">이메일 : </label> <input
						type="email" class="form-control" id="muEmail">
					<button class="btn btn-primary" id="emailBt">인증번호 전송</button>
					<div class="form-check form-check-inline" id="hiddenInput"
						style="display: none;">
						<ul id="hiddenUl">
							<li><input class="form-control" type="number" id="checkNum"></li>
							<li><button class="btn btn-primary" id="submitEmail">인증</button></li>
						</ul>
					</div>
					<div id="hiddenDiv" style="display: none;">인증성공!</div>
				</div>
				<HR>
				<div class="form-group">
					<label class="col-form-label">성별 : </label><br>
					<div class="form-check form-check-inline">
						<input class="form-check-input" type="radio"
							name="inlineRadioOptions" id="male" value="male"> <label
							class="form-check-label" for="inlineRadio1" id="inlineRadio1">남</label>
						<input class="form-check-input" type="radio"
							name="inlineRadioOptions" id="female" value="female"> <label
							class="form-check-label" for="inlineRadio1" id="inlineRadio1">여</label>
					</div>
				</div>
				<HR>
				<div class="form-group">
					<label for="muAddr" class="col-form-label">주소 : </label> <input
						type="text" class="form-control" id="muAddr">
				</div>
				<button class="btn btn-primary btn-block" id="signup" type="button">회원가입</button>
			</div>
		</div>
	</div>
	<script>
		var num;
		var echecked;
		$(document)
				.ready(
						function() {
							$('#signup')
									.on(
											'click',
											function() {
												if ($('#jbCheckInput').val() == 'notclicked') {
													alert('아이디 중복체크를 해주세요');
													$('#muId').val(null);
													$('#muId').focus();
													return;
												}

												if ($('#muId').val().trim().length < 5) {
													alert('아이디는 5글자 이상입니다.');
													$('#muId').val(null);
													$('#muId').focus();
													return;
												}
												if ($('#muName').val().trim().length < 3) {
													alert('닉네임은 3글자 이상입니다.');
													$('#muName').val(null);
													$('#muName').focus();
													return;
												}
												if ($('#muPwd').val().trim().length < 5) {
													alert('비밀번호는 5글자 이상입니다.');
													$('#muPwd').val(null);
													$('#muPwd').focus();
													return;
												}
												if ($('#pwdCheck').val().trim() != $(
														'#muPwd').val().trim()) {
													alert('비밀번호와 동일하지 않습니다. 비밀번호를 다시 입력해주세요');
													$('#muPwd').val(null);
													$('#pwdCheck').val(null);
													$('#muPwd').focus();
													return;
												}
												if ($('#muEmail').val().trim().length < 2) {
													alert('이메일을 작성해주세요');
													$('#muEmail').val(null);
													$('#muEmail').focus();
													return;
												}
												if (echecked != "1") {
													alert('이메일 인증을 진행해주세요');
													$('#muEmail').focus();
													return;
												}
												if ($('#muAddr').val().trim().length < 2) {
													alert('주소를 작성해주세요');
													$('#muAddr').val(null);
													$('#muAddr').focus();
													return;
												}
												var checked = $(
														'input[name="inlineRadioOptions"]:checked')
														.val();
												if (checked == null) {
													alert('성별을 체크해주세요');
													return;
												}

												var signup = {
													muId : $('#muId').val(),
													muName : $('#muName').val(),
													muPwd : $('#muPwd').val(),
													muAge : $('#muAge').val(),
													muGender : $(
															'input[name="inlineRadioOptions"]:checked')
															.val(),
													muEmail : $('#muEmail')
															.val(),
													muAddr : $('#muAddr').val()
												}
												console.log(signup);
												$
														.ajax({
															url : '/mapuser/signup',
															method : 'POST',
															data : signup,
															success : function(
																	res) {
																if (res.result == 'true') {
																	alert('회원가입 성공');
																	location.href = '/views/front/final/publicmap';
																} else {
																	alert('회원가입 실패');
																	return;
																}
															},
															error : function(
																	res) {
																console
																		.log(res);
															}
														})
											})

							$('#emailBt').on('click', function() {
								var inputData = {
									email : $('#muEmail').val()
								}
								console.log($('#muEmail').val());
								console.log(inputData);
								$.ajax({
									url : '/user/MailTest',
									method : 'POST',
									data : inputData,
									success : function(res) {
										alert('인증번호가 전송되었습니다');
										num = res.num;
									},
									error : function(res) {
										console.log(res);
									}
								})
								$('#emailBt').css('display', 'none');
								$('#hiddenInput').css('display', 'block');
							})
							$('#submitEmail').on('click', function() {
								if ($('#checkNum').val() == num) {
									echecked = '1';
									alert('인증에 성공하였습니다!');
									$('#hiddenInput').css('display', 'none');
									$('#hiddenDiv').css('display', 'block');
									$('#muEmail').attr('readonly', 'true');
								} else {
									alert('인증에 실패하였습니다! 번호를 다시 확인해주세요');
									$('#checkNum').val(null);
									$('#checkNum').focus();
									return;
								}
							})
						})

		$('#jbCheck').on('click', function() {
			var user = {
				muId : $('#muId').val()
			}
			$.ajax({
				url : '/mapuser/selectOne',
				method : 'POST',
				data : user,
				success : function(res) {
					console.log(res);
					if (res) {
						$('#hiddenJB').text('사용중인 아이디입니다.');
						$("#hiddenJB").css("color", "red");
						$('#muId').val(null);
						$('#muId').focus();
						return;
					} else {
						$('#hiddenJB').text('사용 가능한 아이디입니다.');
						$("#hiddenJB").css("color", "blue");
						$('#jbCheckInput').val('clicked');
					}
				},
				error : function(res) {
					console.log(res);
				}
			})
		})
	</script>
</body>
</html>