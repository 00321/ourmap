<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>OurMap - 사용자정보 수정</title>
<!-- jQuery -->
<script src="https://code.jquery.com/jquery-3.4.1.min.js"
	integrity="sha256-CSXorXvZcTkaix6Yvo6HppcZGetbYMGWSFlBw8HfCJo="
	crossorigin="anonymous"></script>
<!-- jQuery END-->

<!-- Custom fonts for this template-->
<link href="/sbadmin/vendor/fontawesome-free/css/all.min.css"
	rel="stylesheet" type="text/css">
<!-- Custom styles for this template-->
<link href="/sbadmin/css/sb-admin.css" rel="stylesheet">
<!-- Core plugin JavaScript-->
<script src="/sbadmin/vendor/jquery-easing/jquery.easing.min.js"></script>
<script src="/js/bootstrap.min.js"></script>
<script src="/js/bootstrap.bundle.js"></script>
<link rel="stylesheet" href="/css/bootstrap.min.css" />
<link rel="stylesheet" href="/css/bootstrap-reboot.min.css" />
<link rel="stylesheet" href="/css/bootstrap-grid.min.css" />
<style>
#muAge {
	width: 70px;
	margin-left: 10%;
	margin-right: 60%;
}

#agelabel {
	width: 300px;
	display: flex;
	text-align: center;
}

#emailBt {
	margin-top: 5%;
}

#inlineRadio1 {
	margin-right: 7%;
}

.form-group {
	clear: both;
}

#emailForm {
	height: 100%;
}

#checkNum {
	width: 100px;
	margin-right: 2%;
}

#hiddenInput {
	margin: 2% 2% 2% 0;
	height: 40px;
}

#submitEmail {
	width: 70px;
}

#hiddenUl {
	list-style: none;
}

#hiddenUl li {
	float: left;
	margin-right: 3%;
}
</style>
</head>
<body class="bg-dark">
	<div class="container">
		<div class="card card-login mx-auto mt-5">
			<div class="card-header">
				<h3>회원정보 수정</h3>
			</div>
			<div class="card-body">
				<div class="form-group">
					<label for="muId" class="col-form-label">아이디 : </label> <input
						type="text" class="form-control" id="muId">
				</div>
				<HR>
				<div class="form-group">
					<label for="muName" class="col-form-label">성명 : </label> <input
						type="text" class="form-control" id="muName">
				</div>
				<HR>
				<div class="form-group">
					<label for="muPwd" class="col-form-label">비밀번호 : </label> <input
						type="password" class="form-control" id="muPwd">
				</div>
				<div class="form-group">
					<label for="pwdCheck" class="col-form-label">비밀번호확인 : </label> <input
						type="password" class="form-control" id="pwdCheck">
				</div>
				<HR>
				<div class="form-check form-check-inline">
					<label for="muAge" class="col-form-label" id="agelabel">나이
						: </label> <input type="number" class="form-control" id="muAge">
				</div>
				<HR>
				<div class="form-group" id="emailForm">
					<label for="muEmail" class="col-form-label">이메일 : </label> <input
						type="email" class="form-control" id="muEmail">
					<button class="btn btn-primary" id="emailBt">인증번호 전송</button>
					<div class="form-check form-check-inline" id="hiddenInput"
						style="display: none;">
						<ul id="hiddenUl">
							<li><input class="form-control" type="number" id="checkNum"></li>
							<li><button class="btn btn-primary" id="submitEmail">인증</button></li>
						</ul>
					</div>
					<div id="hiddenDiv" style="display: none;">인증성공!</div>
				</div>
				<HR>
				<div class="form-group">
					<label class="col-form-label">성별 : </label><br>
					<div class="form-check form-check-inline">
						<input class="form-check-input" type="radio"
							name="inlineRadioOptions" id="male" value="남"> <label
							class="form-check-label" for="inlineRadio1" id="inlineRadio1">남</label>
						<input class="form-check-input" type="radio"
							name="inlineRadioOptions" id="female" value="여"> <label
							class="form-check-label" for="inlineRadio1" id="inlineRadio1">여</label>
					</div>
				</div>
				<HR>
				<div class="form-group">
					<label for="muAddr" class="col-form-label">주소 : </label> <input
						type="text" class="form-control" id="muAddr">
				</div>
				<button class="btn btn-primary btn-block" id="update" type="button">update</button>
			</div>
		</div>
	</div>
	<script>
		var num;
		var echecked;
		var user = '${mapS.muNum}';
		$(document)
				.ready(
						function() {
							var userNum = {
								muNum : user
							}

							$.ajax({
								url : '/mapuser/selectOne',
								method : 'POST',
								data : userNum,
								success : function(res) {
									console.log(res);
									$('#muId').val(res.muId);
									$('#muName').val(res.muName);
									$('#muAge').val(res.muAge);
									$('#muPwd').val(res.muPwd);
									$('#pwdCheck').val(res.muPwd);
									if (res.muGender == 'male') {
										$('#male').prop('checked', true);
									} else {
										$('#female').prop('checked', true);
									}
									$('#muEmail').val(res.muEmail);
									$('#muAddr').val(res.muAddr);
								},
								error : function(res) {
									console.log(res);
								}
							})

							$('#update')
									.on(
											'click',
											function() {

												if ($('#muId').val().trim().length < 5) {
													alert('아이디는 5글자 이상입니다.');
													$('#muId').val(null);
													$('#muId').focus();
													return;
												}
												if ($('#muName').val().trim().length < 3) {
													alert('닉네임은 3글자 이상입니다.');
													$('#muName').val(null);
													$('#muName').focus();
													return;
												}
												if ($('#muPwd').val().trim().length < 5) {
													alert('비밀번호는 5글자 이상입니다.');
													$('#muPwd').val(null);
													$('#muPwd').focus();
													return;
												}
												if ($('#pwdCheck').val().trim() != $(
														'#muPwd').val().trim()) {
													alert('비밀번호와 동일하지 않습니다. 비밀번호를 다시 입력해주세요');
													$('#muPwd').val(null);
													$('#pwdCheck').val(null);
													$('#muPwd').focus();
													return;
												}
												if ($('#muEmail').val().trim().length < 2) {
													alert('이메일을 작성해주세요');
													$('#muEmail').val(null);
													$('#muEmail').focus();
													return;
												}
												if ($('#muAddr').val().trim().length < 2) {
													alert('주소를 작성해주세요');
													$('#muAddr').val(null);
													$('#muAddr').focus();
													return;
												}
												var checked = $(
														'input[name="inlineRadioOptions"]:checked')
														.val();
												if (checked == null) {
													alert('성별을 체크해주세요');
													return;
												}

												var updateInfo = {
													muNum : user,
													muId : $('#muId').val(),
													muName : $('#muName').val(),
													muPwd : $('#muPwd').val(),
													muAge : $('#muAge').val(),
													muGender : $(
															'input[name="inlineRadioOptions"]:checked')
															.val(),
													muEmail : $('#muEmail')
															.val(),
													muAddr : $('#muAddr').val()
												}
												$
														.ajax({
															url : '/mapuser/update',
															method : 'POST',
															data : updateInfo,
															success : function(
																	res) {
																if (res.result == 'true') {
																	alert('정보수정 성공');
																	location.href = '/';
																} else {
																	alert('정보수정 실패');
																	return;
																}
															},
															error : function(
																	res) {
																console
																		.log(res);
															}
														})
											})

							$('#emailBt').on('click', function() {
								var inputData = {
									email : $('#muEmail').val()
								}
								console.log($('#muEmail').val());
								console.log(inputData);
								$.ajax({
									url : '/user/MailTest',
									method : 'POST',
									data : inputData,
									success : function(res) {
										alert('인증번호가 전송되었습니다');
										num = res.num;
									},
									error : function(res) {
										console.log(res);
									}
								})
								$('#emailBt').css('display', 'none');
								$('#hiddenInput').css('display', 'block');
							})
							$('#submitEmail').on('click', function() {
								if ($('#checkNum').val() == num) {
									echecked = '1';
									alert('인증에 성공하였습니다!');
									$('#hiddenInput').css('display', 'none');
									$('#hiddenDiv').css('display', 'block');
									$('#muEmail').attr('readonly', 'true');
								} else {
									alert('인증에 실패하였습니다! 번호를 다시 확인해주세요');
									$('#checkNum').val(null);
									$('#checkNum').focus();
									return;
								}
							})
						})
	</script>
</body>
</html>