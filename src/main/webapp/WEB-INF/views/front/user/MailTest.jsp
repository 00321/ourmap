<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>OurMap - 메일 테스트</title>
<!-- jQuery -->
<script src="https://code.jquery.com/jquery-3.4.1.min.js"
	integrity="sha256-CSXorXvZcTkaix6Yvo6HppcZGetbYMGWSFlBw8HfCJo="
	crossorigin="anonymous"></script>
<!-- jQuery END-->
</head>
<body>
	<button id="mailtest">mailtest</button>
	<br> 인증번호 :
	<input type="number" id="mailnum">
	<button id="profit">인증하기</button>
	<script>
		var num;
		$(document).ready(function() {
			$('#mailtest').on('click', function() {
				$.ajax({
					url : '/user/MailTest',
					method : 'POST',
					success : function(res) {
						alert(res.num);
						num = res.num;
					}
				})
			})
			$('#profit').on('click', function() {
				if ($('#mailnum').val() == num) {
					alert('인증성공');
				} else {
					alert('인증실패');
				}
			})
		})
	</script>
</body>
</html>