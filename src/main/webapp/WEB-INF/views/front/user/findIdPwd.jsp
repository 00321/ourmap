<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>OurMap - 아이디 비밀번호 찾기</title>
<!-- jQuery -->
<script src="https://code.jquery.com/jquery-3.4.1.min.js"
	integrity="sha256-CSXorXvZcTkaix6Yvo6HppcZGetbYMGWSFlBw8HfCJo="
	crossorigin="anonymous"></script>
<!-- jQuery END-->

<!-- Custom fonts for this template-->
<link href="/sbadmin/vendor/fontawesome-free/css/all.min.css"
	rel="stylesheet" type="text/css">
<!-- Custom styles for this template-->
<link href="/sbadmin/css/sb-admin.css" rel="stylesheet">
<!-- Core plugin JavaScript-->
<script src="/sbadmin/vendor/jquery-easing/jquery.easing.min.js"></script>

<script src="/js/bootstrap.min.js"></script>
<script src="/js/bootstrap.bundle.js"></script>
<link rel="stylesheet" href="/css/bootstrap.min.css" />
<link rel="stylesheet" href="/css/bootstrap-reboot.min.css" />
<link rel="stylesheet" href="/css/bootstrap-grid.min.css" />
<style>
#muAge {
	width: 70px;
	margin-left: 10%;
	margin-right: 60%;
}

#agelabel {
	width: 300px;
	display: flex;
	text-align: center;
}

#emailBt {
	margin-top: 5%;
}

#inlineRadio1 {
	margin-right: 7%;
}

.form-group {
	clear: both;
}

#emailForm {
	height: 100%;
}

#checkNum {
	width: 100px;
	margin-right: 2%;
}

#hiddenInput {
	margin: 2% 2% 2% 0;
	height: 40px;
}

#submitEmail {
	width: 70px;
}

#hiddenUl {
	list-style: none;
	list-style-position: outside;
	margin: 0;
	padding: 0;
}

#hiddenUl li {
	float: left;
	margin-right: 3%;
}

#select {
	width: 40%;
	margin: 0 30% 0 30%;
}

.roundBox {
	margin: 1% 0 1% 0;
	border-radius: 10px;
	border: 1px solid grey;
}

.container {
	width: 800px;
}

#checkId {
	margin: 0 10% 0 10%;
}
</style>
</head>
<body class="bg-dark">
	<div class="container">
		<div class="card mx-auto mt-5">
			<div class="card-header">
				<h3>아이디/비밀번호 찾기</h3>
			</div>
			<div class="card-body">
				<div id="selectDiv">
					<h4>원하는 서비스를 선택해주세요</h4>
					<HR>
					<div class="custom-control custom-radio">
						<input type="radio" name="jb-radio" id="findId"
							class="custom-control-input" value="id"> <label
							class="custom-control-label" for="findId">아이디 찾기</label>
					</div>
					<HR>
					<div class="custom-control custom-radio">
						<input type="radio" name="jb-radio" id="findPwd"
							class="custom-control-input" value="pwd"> <label
							class="custom-control-label" for="findPwd">비밀번호 찾기</label>
					</div>
					<HR>
					<button class="btn btn-primary btn-block" id="select" type="button">선택</button>
				</div>

				<div class="form-group" id="emailForm" style="display: none">
					<h4 id="idAndPwd">아이디 찾기</h4>
					<HR>
					<label for="muEmail" class="col-form-label" id="changeText">가입시
						입력했던 이메일 주소 : </label> <input type="email" class="form-control"
						id="muEmail">
					<button class="btn btn-primary" id="emailBt">인증번호 전송</button>
					<div class="form-check form-check-inline" id="hiddenInput"
						style="display: none;">
						<ul id="hiddenUl">
							<li><input class="form-control" type="number" id="checkNum"></li>
							<li><button class="btn btn-primary" id="submitEmail">인증</button></li>
						</ul>
					</div>
					<div id="hiddenDiv" style="display: none;">인증성공!</div>
				</div>

				<div id="findIdDiv" style="display: none">
					<h4>아이디 찾기</h4>
					<HR>
					<label for="idOfEmail">이 이메일로 가입된 아이디입니다.</label>
					<ul id="idOfEmail">
					</ul>
					<HR>
					<button class="btn btn-primary"
						onclick="goPage('/views/front/test5')">로그인하러가기</button>
				</div>

				<div id="checkId" style="display: none">
					<h4>비밀번호 찾기</h4>
					<HR>
					<label for="idForPwd" class="col-form-label">아이디를 입력해주세요</label>
					<ul id="hiddenUl">
						<li><input class="form-control" type="text" id="idForPwd"></li>
						<li><button class="btn btn-primary" id="selectId">확인</button></li>
					</ul>
				</div>

				<div id="changePwd" style="display: none">
					<h4>비밀번호 변경</h4>
					<HR>
					<label>새 비밀번호 : </label> <input type="password"
						class="form-control" id="newPwd">
					<HR>
					<label>새 비밀번호 확인 : </label> <input type="password"
						class="form-control" id="checkNewPwd">
					<HR>
					<button class="btn btn-primary" id="pwdChange">확인</button>
				</div>

			</div>
		</div>
	</div>
	<script>
		var num;
		var changePwdmuNum;
		var checking;
		var emailOfId;
		$('#select').on('click', function() {
			if ($('input[name="jb-radio"]:checked').val() == 'id') {
				$('#selectDiv').css('display', 'none');
				$('#emailForm').css('display', 'block');
				checking = 'idchecking';
			} else if ($('input[name="jb-radio"]:checked').val() == 'pwd') {
				$('#selectDiv').css('display', 'none');
				$('#checkId').css('display', 'block');
				checking = 'pwdchecking';
			} else {
				alert('선택해주세요');
			}
		})

		$('#selectId').on('click', function() {
			var yourId = {
				muId : $('#idForPwd').val()
			}
			$.ajax({
				url : '/mapuser/selectOne',
				method : 'POST',
				data : yourId,
				success : function(res) {
					if (res) {
						$('#checkId').css('display', 'none');
						$('#emailForm').css('display', 'block');
						$('#idAndPwd').text('비밀번호 찾기');
						$('#changeText').text('가입시 입력했던 이메일입니다. 인증해주세요');
						$('#muEmail').val(res.muEmail);
						$('#muEmail').attr('readonly', 'true');
						changePwdmuNum = res.muNum;
					} else {
						alert('아이디 없음');
					}
				},
				error : function(res) {
					console.log(res);
				}
			})
		})

		$('#emailBt').on('click', function() {
			if ($('#muEmail').val().trim() < 8) {
				alert('이메일을 작성해주세요');
				$('#muEmail').focus();
				return;
			}
			var inputData = {
				email : $('#muEmail').val()
			}
			console.log($('#muEmail').val());
			emailOfId = $('#muEmail').val();
			$.ajax({
				url : '/user/MailTest',
				method : 'POST',
				data : inputData,
				success : function(res) {
					alert('인증번호가 전송되었습니다');
					num = res.num;
				},
				error : function(res) {
					console.log(res);
				}
			})
			$('#emailBt').css('display', 'none');
			$('#hiddenInput').css('display', 'block');
		})
		$('#submitEmail').on('click', function() {
			if ($('#checkNum').val() == num) {
				alert('인증에 성공하였습니다!');
				if (checking == 'pwdchecking') {
					$('#emailForm').css('display', 'none');
					$('#changePwd').css('display', 'block');
				} else if (checking == 'idchecking') {
					findIdOfEmail();
					$('#emailForm').css('display', 'none');
					$('#findIdDiv').css('display', 'block');
				}
			} else {
				alert('인증에 실패하였습니다! 번호를 다시 확인해주세요');
				$('#checkNum').val(null);
				$('#checkNum').focus();
				return;
			}
		})

		$('#pwdChange').on('click', function() {
			if ($('#checkNewPwd').val().trim() != $('#newPwd').val().trim()) {
				alert('비밀번호가 맞지 않습니다. 다시 입력해주세요');
				$('#checkNewPwd').val(null);
				$('#checkNewPwd').focus();
				return;
			}

			var changePwd = {
				muNum : changePwdmuNum,
				muPwd : $('#newPwd').val()
			}
			$.ajax({
				url : '/mapuser/update',
				method : 'POST',
				data : changePwd,
				success : function(res) {
					if (res.result == 'true') {
						alert('변경되었습니다. 새로운 비밀번호로 로그인해주세요.');
						location.href = '/views/front/test5';
					}
				},
				error : function(res) {
					console.log(res);
				}
			})
		})

		function findIdOfEmail() {
			$
					.ajax({
						url : '/mapuser/getList',
						method : 'POST',
						data : {
							muEmail : emailOfId
						},
						success : function(res) {
							console.log(res);
							console.log(res.length);
							for (var i = 0; i < res.length; i++) {
								$('#idOfEmail').append(
										'<li>' + res[i].mu_id + '</li>');
							}
						},
						error : function(res) {
							console.log(res);
						}
					})
		}

		function goPage(f) {
			location.href = f;
		}
	</script>
</body>
</html>