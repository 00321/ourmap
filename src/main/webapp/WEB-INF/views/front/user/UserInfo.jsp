<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>사용자 정보</title>
<!-- jQuery -->
<script src="https://code.jquery.com/jquery-3.4.1.min.js"
	integrity="sha256-CSXorXvZcTkaix6Yvo6HppcZGetbYMGWSFlBw8HfCJo="
	crossorigin="anonymous"></script>
<!-- jQuery END-->
<link rel="stylesheet" href="/css/bootstrap.min.css">
<script src="/js/bootstrap.min.js"></script>
<!-- fontawsome -->
<script src="https://kit.fontawesome.com/86fb79723e.js"
	crossorigin="anonymous"></script>
<!-- fontawsome end -->
<style>
body {
	background-color: #FFFFFF;
	color: rgba(0,0,0,.8);
	width:600px;
	height:500px;
}

.container {
	margin: 5% 5% 5% 5%;
	width: 100%;
	height: 100%;
}

#header {
	width: 100%;
	height: 1.5rem;
}

#content {
	clear: both;
}

#userImg {
	color: black;
}

#profileImg {
	margin: 3% 3% 3% 3%;
	float: left;
	width: 27%;
	text-align: center;
}

#info {
	margin: 0 3% 3% 3%;
	float: left;
	width: 61%;
}

#infoUl {
	list-style: none;
}

HR {
	background-color: white;
}

li {
	margin: 2% 0 2% 0;
}

#updateBt {
	float: right;
}

label {
	margin: 0 2% 0 0;
}

#resign {
	float: right;
}
#backcol{
	background-color: #FFFFFF;
}
</style>
</head>
<body>
	<div class="container">
		<h3>
			회원 정보
			<button class="btn btn-primary" id="updateBt">수정</button>
		</h3>

		<HR>
		<div id="header">
			<div id="profileImg">
				<i class="far fa-user fa-7x" id="userImg"></i>
			</div>
			<div id="info">
				<ul id="infoUl">
					<li><label>ID : </label><span id="muId"></span></li>
					<li><label>이름 : </label><span id="muName"></span></li>
					<li><label>나이 : </label><span id="muAge"></span></li>
					<li><label>이메일 : </label><span id="muEmail"></span></li>
					<li><label>주소 : </label><span id="muAddr"></span></li>
				</ul>
			</div>
		</div>
		<div id="content">
			<HR>
			<div>
			<!-- 회원탈퇴 모달 -->
				<div class="modal fade" id="exampleModalCenter" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle"
					aria-hidden="true">
					<div class="modal-dialog modal-dialog-centered" role="document">
						<div class="modal-content" id="backcol">
							<div class="modal-header">
								<h5 class="modal-title" id="exampleModalCenterTitle">비밀번호 확인</h5>
								<button type="button" class="close" data-dismiss="modal" aria-label="Close">
									<span aria-hidden="true">&times;</span>
								</button>
							</div>
							<div class="modal-body">
								<label for="rsPwdCheck">회원님의 비밀번호를 작성해주세요</label>
								<input type="password" id="rsPwdCheck" class="form-control">
							</div>
							<div class="modal-footer">
								<button type="button" class="btn btn-primary" id="really">확인</button>
								<button type="button" class="btn btn-secondary" data-dismiss="modal">닫기</button>
							</div>
						</div>
					</div>
				</div>
				<!-- 회원탈퇴 모달 끝 -->
			</div>
		</div>
		<HR>
		<div id="popupFooter">
			<button class="btn btn-warning" id="resign" data-toggle="modal" data-target="#exampleModalCenter">회원 탈퇴</button>
		</div>
	</div>
	<script>
	var number = '${mapS.muNum}';
	
	$(document).ready(function(){
		      // 팝업 창 크기를 HTML 크기에 맞추어 자동으로 크기를 조정하는 함수.
		      var strWidth;
		      var strHeight;

		      //innerWidth / innerHeight / outerWidth / outerHeight 지원 브라우저 
		      if ( window.innerWidth && window.innerHeight && window.outerWidth && window.outerHeight ) {
		          strWidth = $('body').outerWidth() + (window.outerWidth - window.innerWidth);
		          strHeight = $('body').outerHeight() + (window.outerHeight - window.innerHeight);
		      }
		      else {
		          var strDocumentWidth = $(document).outerWidth();
		          var strDocumentHeight = $(document).outerHeight();

		          window.resizeTo ( strDocumentWidth, strDocumentHeight );

		          var strMenuWidth = strDocumentWidth - $(window).width();
		          var strMenuHeight = strDocumentHeight - $(window).height();

		          strWidth = $('body').outerWidth() + strMenuWidth;
		          strHeight = $('body').outerHeight() + strMenuHeight;
		      }

		      //resize 
		      window.resizeTo( strWidth, strHeight );
		
		
		var usernum = {
				muNum:number
		}
		$.ajax({
			url:'/mapuser/selectOne',
			method:'POST',
			data:usernum,
			success:function(res){
				console.log(res);
				$('#muId').text(res.muId);
				$('#muName').text(res.muName);
				$('#muEmail').text(res.muEmail);
				$('#muAge').text(res.muAge);
				$('#muAddr').text(res.muAddr);
			},
			error:function(res){
				console.log(res);
			}
		})
	})
	
		$('#updateBt').on('click', function() {
			window.close();
			opener.location.href = '/views/front/user/infoUpdate';
		})

		$('#really').on('click', function() {
			var passwd = {
					muNum:number,
					muPwd:$('#rsPwdCheck').val()
			}
			$.ajax({
				url:'/mapuser/selectOne',
				method:'POST',
				data:passwd,
				success:function(res){
					if(res){
						var result = confirm('정말 회원탈퇴를 하시겠습니까?');
						
						if(result){
							deleteUser();
						}else{
							alert('안함');
						}
					}else{
						alert('비밀번호를 다시 확인해주세요');
					}
				},
				error:function(res){
					console.log(res.msg);
				}
			})
		})
		
		function deleteUser(){
			var usernum = {
					muNum:number
			}
			$.ajax({
				url:'/mapuser/delete',
				method:'POST',
				data:usernum,
				success:function(res){
					if(res.result = 'true'){
						alert('탈퇴되었습니다.');
						window.close();
						logout();
						opener.location.href='/views/front/test5';
					}else{
						alert('탈퇴가 제대로 이루어지지 않았습니다.');
						return;
					}
				},
				error:function(res){
					
				}
			})
		}
		
		function logout(){
			$.ajax({
				url:'/mapuser/logout',
				method:'POST',
				success:function(res){
					alert(res.msg);
				},
				error:function(res){
					console.log(res);
				}
			})
		}
		
	
	</script>
</body>
</html>