<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>OurMap - 상세정보</title>
<!-- jQuery -->
<script src="https://code.jquery.com/jquery-3.4.1.min.js"
	integrity="sha256-CSXorXvZcTkaix6Yvo6HppcZGetbYMGWSFlBw8HfCJo="
	crossorigin="anonymous"></script>
<!-- jQuery END-->
<link rel="stylesheet" href="/css/bootstrap.min.css">
<script src="/js/bootstrap.min.js"></script>
<!-- fontawsome -->
<script src="https://kit.fontawesome.com/86fb79723e.js"
	crossorigin="anonymous"></script>
<!-- fontawsome end -->
<style>
body {
	background-color: #FFFFFF;
	color: rgba(0, 0, 0, .8);
}

.container {
	margin: 5% 5% 5% 5%;
	width: 100%;
	height: 100%;
}

#header {
	width: 100%;
	height: 100%;
}

#content {
	clear: both;
	border-top: 1px solid white;
	border-bottom: 1px solid white;
}

#userImg {
	color: white;
}

#profileImg {
	margin: 3% 3% 3% 3%;
	float: left;
	text-align: center;
	width: 90%;
}

#infomation {
	margin: 0 3% 3% 3%;
	width: 100%;
}

#infoUl {
	list-style: none;
	padding: 0;
}

HR {
	background-color: white;
}

li {
	margin: 2% 0 2% 0;
}

#deleteBt, #closeBt, #saveBt, #updateBt {
	float: right;
	margin-left: 10px;
}

label {
	margin: 0 2% 0 0;
}

#resign {
	float: right;
}

#backcol {
	background-color: #343A40;
}

#pmContent {
	width: 80%;
	margin: 1% 3% 1% 3%;
	background-color: transparent;
	resize: none;
	border: 0;
	color: black;
}

#pmRoadad {
	width: 80%;
	margin: 1% 3% 1% 3%;
	background-color: transparent;
	resize: none;
	border: 0;
	color: black;
}

#pmContent:focus {
	outline: none;
}

#popupFooter {
	margin: 1% 3% 1% 3%;
}

#writeCMT {
	border: 1px solid rgba(0, 0, 0, .7);
	border-radius: 5px;
	background-color: white;
}

#commentTA {
	background-color: transparent;
	resize: none;
	border: 0;
	color: white;
	border-bottom: 1px solid gray;
	color: black;
}

#star {
	width: 80%;
	flex: 1;
}

#star i {
	color: black;
	margin: 2% 0 1% 1%;
}

#starandsubmit {
	display: flex;
}

#cmtsubmit {
	width: 20%;
}

#cmtsubmit button {
	width: 100%;
}
</style>
</head>
<body>
	<div class="container">
		<h3>
			<i class="fas fa-info-circle"></i> 상세정보
			<button class="btn btn-primary" id="closeBt" onclick="closeWindow()">닫기</button>
			<button class="btn btn-primary" id="deleteBt"
				onclick="deleteConfirm()">삭제</button>
			<button class="btn btn-primary" id="updateBt"
				onclick="updateMarker()">수정</button>
			<button class="btn btn-primary" id="saveBt" onclick="saveMarker()">개인지도에
				저장</button>
		</h3>

		<HR>
		<div id="header">
			<div id="profileImg">
				<img id="pmFile"
					style="width: 100%; height: 300px; border: 1px solid white;">
			</div>

		</div>
		<div id="content">
			<div id="infomation">
				<ul id="infoUl">
					<li><h4 id="pmTitle">제목 들어갈 곳</h4></li>
					<li><label for="pmContent"><i
							class="fas fa-map-marked-alt"> 상세주소 :</i></label><br> <textarea
							class="form-control" id="pmRoadad" readonly="readonly"></textarea>
					</li>
					<li><label for="pmContent"><i class="far fa-edit">
								이 장소는?</i></label><br> <textarea class="form-control" id="pmContent"
							readonly="readonly">설명넣을곳</textarea></li>
				</ul>
			</div>
		</div>
		<div id="popupFooter">
			<h4>리뷰를 만들면 들어갈 곳</h4>
			<div id="writeCMT">
				<textarea class="form-control" id="commentTA"></textarea>
				<div id="starandsubmit">
					<div id="star">
						<i class="far fa-star"></i> <i class="far fa-star"></i> <i
							class="far fa-star"></i> <i class="far fa-star"></i> <i
							class="far fa-star"></i>
					</div>
					<div id="cmtsubmit">
						<button class="btn btn-primary">등록하기</button>
					</div>
				</div>
			</div>
			<div id="comment">5개씩 코멘트 달릴 예정</div>
		</div>
	</div>
	<script type="text/javascript"
		src="//dapi.kakao.com/v2/maps/sdk.js?appkey=a3fdc8c4b9ff7aa04c7acecbe9c1b0d9&libraries=services"></script>
	<script>
var sessionNum = '${mapS.muNum}';
	
//주소-좌표 변환 객체를 생성합니다
var geocoder = new kakao.maps.services.Geocoder();

$('#pmContent').each(function () {
	this.setAttribute('style', 'height:' + (this.scrollHeight) + 'px;overflow-y:hidden;');
}).on('input', function () {
	this.style.height = 'auto';
	this.style.height = (this.scrollHeight) + 'px';
});


$(document).ready(function(){
	var number = ${param.pmNum};
	$.ajax({
		url:'/pmap/one?pmNum=${param.pmNum}',
		method:'GET',
		success:function(res){
			console.log(res);
			$('#pmFile').attr('src','https://ourmapimg.s3.ap-northeast-2.amazonaws.com/img/'+res.pmFile);
			$('#pmTitle').text(res.pmTitle + ' ('+ res.pmCategoryName + ')');
			$('#pmContent').text(res.pmContent);
			if(res.pmRoadad != ''){
			$('#pmRoadad').val('도로명 주소 : ' + res.pmRoadad + '\n지번 주소 : ' + res.pmOldad);
			} else {
				$('#pmRoadad').val('지번 주소 : ' + res.pmOldad);
			}
			console.log(sessionNum);
			console.log(res.muNum);
			if(sessionNum != res.muNum && sessionNum != ''){
				$('#updateBt').css('display','none');
				$('#deleteBt').css('display','none');
				$('#saveBt').css('display','block');
			} else if(sessionNum != res.muNum){
				$('#updateBt').css('display','none');
				$('#deleteBt').css('display','none');
				$('#saveBt').css('display','none');
			} else if(sessionNum == res.muNum){
				$('#updateBt').css('display','block');
				$('#deleteBt').css('display','block');
				$('#saveBt').css('display','none');
			}
			},
		error:function(res){
			console.log(res);
		}
	})
})

function deleteConfirm(){
	var con = confirm("정말 삭제하시겠습니까?'\n공유한 자료도 함께 삭제됩니다.");
	 if(con) {
		 deleteMarker();
	 }	
}

function deleteMarker(){
	var pmNum = {
			pmNum:parseInt(${param.pmNum})
	}	
	pmNum = JSON.stringify(pmNum)
	$.ajax({
		url:'/pmap/delete',
		method:'DELETE',
		data:pmNum,
		beforeSend : function(xhr) {
			xhr.setRequestHeader('Content-type',
					'application/json;charset=utf-8')
		},
		success:function(res){
			if(res.result = 'true'){
				alert('삭제되었습니다.');
				opener.parent.location.reload();
				window.close();
			}else{
				alert('삭제가 제대로 이루어지지 않았습니다.');
			}
		},
		error:function(res){
		}
	})
}

function updateMarker() {
	location.href = '/views/front/update?pmNum='+${param.pmNum};
}

function closeWindow(){
	opener.parent.location.reload();
	window.close();
}

var shareList;
function saveMarker() {	
	$.ajax({
		url : '/pmap/one?pmNum=${param.pmNum}',
		method : 'GET',
		success : function(res) {
			console.log(res);
			shareList = res;
			downloadToPrivateMap();
		},
		error : function(res) {
			console.log(res);
		}
	})
	
}

function downloadToPrivateMap(){
	var con = confirm("개인 지도에 저장하시겠습니까?");
	if(con) {
	var formData = new FormData();
	formData.append('muNum', sessionNum);
	formData.append('pmLatitude', shareList.pmLatitude);
	formData.append('pmLongitude', shareList.pmLongitude);
	formData.append('pmTitle', shareList.pmTitle);
	formData.append('pmContent', shareList.pmContent);
	formData.append('pmFile', shareList.pmFile);
	formData.append('pmCategory', shareList.pmCategory);
	formData.append('pmCategoryName', shareList.pmCategoryName);
	formData.append('pmRoadad', shareList.pmRoadad);
	formData.append('pmOldad', shareList.pmOldad);
	
	console.log(formData);

	var xhr = new XMLHttpRequest();
	xhr.open('POST', '/pmap/download');
	xhr.onreadystatechange = function() {
		if (xhr.readyState == xhr.DONE) {
			if (xhr.status == 200) {
				console.log(xhr.responseText);
				var msg = JSON.parse(xhr.responseText);
				alert(msg.msg);
				opener.parent.location.reload();
				window.close();
				}
		}
	}
	xhr.send(formData);
	}
}
</script>
</body>
</html>