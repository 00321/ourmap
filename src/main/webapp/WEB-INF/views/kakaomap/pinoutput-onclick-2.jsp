<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Insert title here</title>
<script
  src="https://code.jquery.com/jquery-3.4.1.min.js"
  integrity="sha256-CSXorXvZcTkaix6Yvo6HppcZGetbYMGWSFlBw8HfCJo="
  crossorigin="anonymous"></script> 
<!-- jQuery END-->
<link rel="stylesheet" href="/css/bootstrap.min.css">
<script src="/js/bootstrap.js"></script>
<script src="/js/bootstrap.min.js"></script>
<!-- fontawsome -->
<script src="https://kit.fontawesome.com/86fb79723e.js"
	crossorigin="anonymous"></script>
<!-- fontawsome end -->
<link rel="stylesheet" href="/css/mainMapCss.css">
<link rel="stylesheet" href="/css/pinInput.css">
<link rel="stylesheet" href="/css/pinOutput.css">

<style>
#imgAttach{
	opacity:0.0;
	position:absolute;
	top:0;
	left:0;
	z-index:1;
	float:left;
	width:277px;
	height:150px;
}
#LoadImg{
	position:relative;
}
</style>
</head>
<body>
<div id="map" style="width:100%;height:700px;"></div>
<p><em>마커를 클릭해주세요!</em></p> 
<div id="clickLatlng"></div>

<script type="text/javascript" src="//dapi.kakao.com/v2/maps/sdk.js?appkey=a3fdc8c4b9ff7aa04c7acecbe9c1b0d9"></script>
<script>
var mapContainer = document.getElementById('map'), // 지도를 표시할 div 
    mapOption = { 
        center: new kakao.maps.LatLng(33.450701, 126.570667), // 지도의 중심좌표
        level: 3 // 지도의 확대 레벨
    };

var map = new kakao.maps.Map(mapContainer, mapOption); // 지도를 생성합니다
  
// 마커를 표시할 위치입니다 
var position =  new kakao.maps.LatLng(33.450701, 126.570667);

// 마커를 생성합니다
var marker = new kakao.maps.Marker({
  position: position,
  clickable: true // 마커를 클릭했을 때 지도의 클릭 이벤트가 발생하지 않도록 설정합니다
});


// 아래 코드는 위의 마커를 생성하는 코드에서 clickable: true 와 같이
// 마커를 클릭했을 때 지도의 클릭 이벤트가 발생하지 않도록 설정합니다
// marker.setClickable(true);

// 마커를 지도에 표시합니다.
marker.setMap(map);

// 마커를 클릭했을 때 마커 위에 표시할 인포윈도우를 생성합니다

var iwContent ='<div class="container">'+	
'<div class="overlayboxinput">'+
'<div class="placeName">'+
	'<label id="whiteCol">'+
		'작성양식'+
	'</label>'+
	'<input type="text" class="form-control" id="title"	aria-describedby="basic-addon3" placeholder="제목">'+
'</div>'+
'<div class="first">'+
	'<div class="center-block">'+
		'<div class="triangle text">'+
			'<div class="filebox">'+
				'<img id="LoadImg" width="277px" height="150px">'+
				'<input type="file" id="imgAttach" name="imgAttach" onchange="LoadImg(this);">'+
				'<span>이미지 업로드</span>'+
			'</div>'+
		'</div>'+
	'</div>'+
'</div>'+
'<label id="whiteCol">카테고리 : </label>'+
'<select class="form-control" id="category">'+
	'<option>종류를 골라주세요</option>'+
	'<option>음식점</option>'+
	'<option>병원</option>'+
	'<option>술집</option>'+
'</select>'+
'<HR>'+
'<textarea class="form-control" id="content" aria-describedby="basic-addon3" placeholder="설명"style="resize: none;"></textarea>'+
'<HR>'+
'<button id="tButton" class="btn btn-dark" onclick="saveMarker()">저장</button>'+
'</div>'+
'</div>';
    iwRemoveable = true; // removeable 속성을 ture 로 설정하면 인포윈도우를 닫을 수 있는 x버튼이 표시됩니다

// 인포윈도우를 생성합니다
var infowindow = new kakao.maps.InfoWindow({
    content : iwContent,
    removable : iwRemoveable
});

// 마커에 클릭이벤트를 등록합니다

kakao.maps.event.addListener(marker, 'click', function() {
      // 마커 위에 인포윈도우를 표시합니다
      infowindow.open(map, marker);  
});

//지도에 클릭 이벤트를 등록합니다
//지도를 클릭하면 마지막 파라미터로 넘어온 함수를 호출합니다
kakao.maps.event.addListener(map, 'click', function(mouseEvent) {        
    
    // 클릭한 위도, 경도 정보를 가져옵니다 
    var latlng = mouseEvent.latLng; 
    console.log(latlng);
    
    // 마커 위치를 클릭한 위치로 옮깁니다
    marker.setPosition(latlng);
    
    var message = '클릭한 위치의 위도는 ' + latlng.getLat() + ' 이고, ';
    message += '경도는 ' + latlng.getLng() + ' 입니다';
    
    var resultDiv = document.getElementById('clickLatlng'); 
    resultDiv.innerHTML = message;
    infowindow.close();

    
});

function saveMarker(){
	var latLng = document.getElementById('clickLatlng').innerHTML;
	console.log(latLng);
	var abc = latLng.split('');
	var def = '';
	for (var i=0;i<abc.length;i++){
		if(parseInt(abc[i])||abc[i]=='.'||abc[i]=='\,'||abc[i]==0){
			def += (abc[i]+'');
		}
	}
	var ghi=def.split(',');
	console.log(def);
	var img = document.querySelector("#imgAttach");
	var title = document.querySelector("#title");
	var category = document.querySelector("#category");
	var content = document.querySelector("#content");
	
	var formData = new FormData();
	formData.append('pmTitle', title.value);
	formData.append('pmContent', content.value);
	formData.append('pmCategory', category.value);
	if(img.files[0]){
	formData.append('pmFileItem', img.files[0]);
	}
	formData.append('pmLatitude', ghi[0]);
	formData.append('pmLongitude', ghi[1]);
	console.log(formData);
	console.log(img.files[0]);
	
	var xhr = new XMLHttpRequest();
	xhr.open('POST', '/pmap/insert2');
	xhr.onreadystatechange = function() {
		if (xhr.readyState == xhr.DONE) {
			if (xhr.status == 200) {
				console.log(xhr.responseText);
				var msg = JSON.parse(xhr.responseText);
				alert(msg.msg);
				infowindow.close();
				location.href='/views/kakaomap/pinoutput-onclick';
				infowindow = new kakao.maps.InfoWindow({
				    content : iwContent,
				    removable : iwRemoveable
				});
				
			}
		}
	}
	xhr.send(formData);
}



window.onload = function(){
	var xhr = new XMLHttpRequest();
	xhr.open('GET', '/pmap/list2');
	xhr.onreadystatechange = function() {
		if (xhr.readyState == xhr.DONE) {
			if (xhr.status == 200) {
				function makeOverListener(map, marker, infowindow) {
				    return function() {
				        infowindow.open(map, marker);
				    };
				}
				function makeOutListener(infowindow) {
				    return function() {
				        infowindow.close();
				    };
				}
				var privateMapList = JSON.parse(xhr.responseText);
				var markers = [];
				var overlays = [];
				for (var i=0;i<privateMapList.length;i++){
					var pmLat = privateMapList[i].pmLatitude;
					var pmLng = privateMapList[i].pmLongitude;
					console.log(pmLat);
					console.log(pmLng);
					console.log(privateMapList[i]);
					var position = new kakao.maps.LatLng(pmLat, pmLng);
					var marker = new kakao.maps.Marker({
				        position: position,
				    });
					markers.push(marker);
					markers[i].setMap(map); 
					privateMapList.forEach(function(element){
						var csContent = document.createElement('div');
						csContent.className = 'overlay';
						csContent.innerHTML = '<div class="container">'+ 
						'<div class="overlayboxoutput">'+
						'<div class="placeName">'+
							'<label id="whiteCol">'+
							'<input type="hidden" class="form-control" id="pmnum"	aria-describedby="basic-addon3" value=' + privateMapList[i].pmNum + '>'+
							'<input type="hidden" class="form-control" id="munum"	aria-describedby="basic-addon3" value=' + privateMapList[i].muNum + '>'+
							privateMapList[i].pmTitle +
							'</label>'+
							'<div class="close" onclick="closeOverlay()" title="닫기">'+
							'<i class="fas fa-times fa-2x"></i>'+
							'</div>' +
						'</div>'+
						'<div class="first">'+
							'<div class="center-block">'+
									'<div id="addr_catg_bt">'+
									'<div class="filebox">'+
										'<img id="LoadImg" width="130px" height="100px">'+
										'</div>'+
										'<ul id="outputList">'+
											'<li>주소 : <span id="pmAddr">주소들어갈곳</span></li>'+
											'<li>카테고리 : <span id="pmCat">'+
											privateMapList[i].pmCategory +
											'</span></li>'+
											'<li>'+
												'<button class="btn btn-outline-light" onclick="updateMarker()">정보수정</button>'+
												'<button class="btn btn-outline-light">공유</button>'+
												'<button class="btn btn-outline-light" onclick="deleteMarker()">삭제</button>'+
											'</li>'+
										'</ul>'+
									'</div>'+
							'</div>'+
						'</div>'+
						'<div class="footer">'+
								'<div class="footerchild"><a onclick="infopopUp()"><i class="fas fa-info"></i></a></div>'+
								'<div class="footerchild"><a><i class="fas fa-star"></i></a></div>'+
								'<div class="footerchild third"><a><i class="fas fa-share-alt"></i></a></div>'+
						'</div>'+
						'</div>'+
						'<div class="arrow-down" > </div>'+
						'</div>';
						

						var overlay = new kakao.maps.CustomOverlay({
						    content: csContent,
						    map: map,
						    position: marker.getPosition(),
						    xAnchor: 0.5,
						    yAnchor: 1.17,
						    clickable: true
						});
						
						overlay.setMap(null);
						console.log(overlay.position);
						kakao.maps.event.addListener(marker, 'click', function() {
							overlay.setMap(map);
						});
						

						 function closeOverlay(f) {
							    overlay.setMap(null);     
							}
						
						kakao.maps.event.addListener(map, 'click', function(mouseEvent){
							overlay.setMap(null);
							infowindow.close();
						});
					})
				}
			}
		}
	}
	xhr.send();
}

function createOverlays() {
    var overlay = new kakao.maps.CustomOverlay({});
    overlays.push(overlay);
}

function removeOverlays() {
    overlays.forEach(function(overlay) {
        overlay.setMap(null);
    });
}


function updateMarker(){
	var num = document.querySelector("#pmnum");
	var img = document.querySelector("#imgAttach");
	var title = document.querySelector("#title");
	var category = document.querySelector("#category");
	var content = document.querySelector("#content");
	
	var formData = new FormData();
	formData.append('pmNum', num.value);
	formData.append('pmTitle', title.value);
	formData.append('pmContent', content.value);
	formData.append('pmCategory', category.value);
	if(img.files[0]){
	formData.append('pmFileItem', img.files[0]);
	}
	console.log(formData);
	console.log(img.files[0]);
	
	var xhr = new XMLHttpRequest();
	xhr.open('PUT', '/pmap/update2');
	xhr.onreadystatechange = function() {
		if (xhr.readyState == xhr.DONE) {
			if (xhr.status == 200) {
				console.log(xhr.responseText);
				var msg = JSON.parse(xhr.responseText);
				alert(msg.msg);
				infowindow.close();
				location.href='/views/kakaomap/pinoutput-onclick';
				overlay = new kakao.maps.CustomOverlay({
				    content: csContent,
				    map: map,
				    position: marker.getPosition(), 
				    xAnchor: 0.5,
				    yAnchor: 1.25
				});		
			}
		}
	}
	xhr.send(formData);
}

function deleteMarker(){
	var pmNum = {
			pmNum:parseInt(document.querySelector("#pmnum").value)
	}
	console.log(pmNum)
	
	pmNum = JSON.stringify(pmNum)
	console.log(pmNum)
	$.ajax({
		url:'/pmap/delete2',
		method:'DELETE',
		data:pmNum,
		beforeSend : function(xhr) {
			xhr.setRequestHeader('Content-type',
					'application/json;charset=utf-8')
		},
		success:function(res){
			if(res.result = 'true'){
				alert('삭제되었습니다.');
				location.href='/views/kakaomap/pinoutput-onclick';
			}else{
				alert('삭제가 제대로 이루어지지 않았습니다.');
			}
		},
		error:function(res){
		}
	})
}

		function LoadImg(value){
			if(value.files && value.files[0]){
				var reader = new FileReader();
				reader.onload = function(e){
					$('#LoadImg').attr('src',e.target.result);
				}
				reader.readAsDataURL(value.files[0]);
			}
		}
		
		function infopopUp(){
			window.open('/views/front/infoPopup','window','width=600','height=600');
		}
	</script>
</body>
</html>