<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Insert title here</title>
<script
  src="https://code.jquery.com/jquery-3.4.1.min.js"
  integrity="sha256-CSXorXvZcTkaix6Yvo6HppcZGetbYMGWSFlBw8HfCJo="
  crossorigin="anonymous"></script> 
<!-- jQuery END-->
<link rel="stylesheet" href="/css/bootstrap.min.css">
<!-- <link rel="stylesheet" href="/css/style.css"> -->
<script src="/js/bootstrap.js"></script>
<script src="/js/bootstrap.min.js"></script>
<!-- fontawsome -->
<script src="https://kit.fontawesome.com/86fb79723e.js"
	crossorigin="anonymous"></script>
<!-- fontawsome end -->

<style>
.overlaybox {
	position: relative;
	width: 300px;
	height: 100%;
	padding: 15px 10px;
	background-color: #808080;
}

.overlaybox div, ul {
	overflow: hidden;
	margin: 0;
	padding: 0;
}

.overlaybox li {
	list-style: none;
}

.overlaybox .placeName {
	color: #fff;
	font-size: 16px;
	font-weight: bold;
	margin-bottom: 8px;
}

.overlaybox .first {
	position: relative;
	width: 277px;
	height: 100%;
	margin-bottom: 8px;
	margin-right: 0px;
}

.first .text {
	color: #fff;
	font-weight: bold;
}

.first .placeContext {
	position: absolute;
	width: 100%;
	bottom: 0;
	background: rgba(0, 0, 0, 0.4);
	padding: 7px 15px;
	font-size: 14px;
}

.overlaybox ul {
	width: 230px;
}

.overlaybox li {
	position: relative;
	margin-bottom: 2px;
	background: #2b2d36;
	padding: 5px 10px;
	color: #aaabaf;
	line-height: 1;
}

.overlaybox li span {
	display: inline-block;
}

.overlaybox li .title {
	font-size: 13px;
}

.overlaybox li .up {
	background-position: 0 -40px;
}

.overlaybox li .down {
	background-position: 0 -60px;
}

.overlaybox li .count {
	position: absolute;
	right: 15px;
	font-size: 15px;
}

.overlaybox li:hover {
	color: #fff;
	background: #d24545;
}

.overlaybox li:hover .up {
	background-position: 0 0px;
}

.overlaybox li:hover .down {
	background-position: 0 -20px;
}

#imgDiv {
	height: 200px;
}
#imgAttach{
	opacity:0.0;
	position:absolute;
	top:0;
	left:0;
	z-index:1;
	float:left;
	width:277px;
	height:150px;
}
#LoadImg{
	position:relative;
}
.filebox{
	position:relative;
}
#whiteCol{
	color:white;
}
</style>
</head>
<body>
<div id="map" style="width:100%;height:700px;"></div>
<p><em>마커를 클릭해주세요!</em></p> 
<div id="clickLatlng"></div>

<script type="text/javascript" src="//dapi.kakao.com/v2/maps/sdk.js?appkey=a3fdc8c4b9ff7aa04c7acecbe9c1b0d9"></script>
<script>
var mapContainer = document.getElementById('map'), // 지도를 표시할 div 
    mapOption = { 
        center: new kakao.maps.LatLng(33.450701, 126.570667), // 지도의 중심좌표
        level: 3 // 지도의 확대 레벨
    };

var map = new kakao.maps.Map(mapContainer, mapOption); // 지도를 생성합니다
  
// 마커를 표시할 위치입니다 
var position =  new kakao.maps.LatLng(33.450701, 126.570667);

// 마커를 생성합니다
var marker = new kakao.maps.Marker({
  position: position,
  clickable: true // 마커를 클릭했을 때 지도의 클릭 이벤트가 발생하지 않도록 설정합니다
});


// 아래 코드는 위의 마커를 생성하는 코드에서 clickable: true 와 같이
// 마커를 클릭했을 때 지도의 클릭 이벤트가 발생하지 않도록 설정합니다
// marker.setClickable(true);

// 마커를 지도에 표시합니다.
marker.setMap(map);

// 마커를 클릭했을 때 마커 위에 표시할 인포윈도우를 생성합니다
var iwContent ='<div class="container">'+	
'<div class="overlaybox">'+
'<div class="placeName">'+
	'<label id="whiteCol">'+
		'작성양식'+
	'</label>'+
	'<input type="text" class="form-control" id="title"	aria-describedby="basic-addon3" placeholder="제목">'+
'</div>'+
'<div class="first">'+
	'<div class="center-block">'+
		'<div class="triangle text">'+
			'<div class="filebox">'+
				'<img id="LoadImg" width="277px" height="150px">'+
				'<input type="file" id="imgAttach" name="imgAttach" onchange="LoadImg(this);">'+
				'<span>이미지 업로드</span>'+
			'</div>'+
		'</div>'+
	'</div>'+
'</div>'+
'<label id="whiteCol">카테고리 : </label>'+
'<select class="form-control" id="category">'+
	'<option>종류를 골라주세요</option>'+
	'<option>음식점</option>'+
	'<option>병원</option>'+
	'<option>술집</option>'+
'</select>'+
'<HR>'+
'<textarea class="form-control" id="content" aria-describedby="basic-addon3" placeholder="설명"style="resize: none;"></textarea>'+
'<HR>'+
'<button id="tButton" class="btn btn-dark" onclick="saveMarker()">저장</button>'+
'</div>'+
'</div>';
    iwRemoveable = true; // removeable 속성을 ture 로 설정하면 인포윈도우를 닫을 수 있는 x버튼이 표시됩니다

// 인포윈도우를 생성합니다
var infowindow = new kakao.maps.InfoWindow({
    content : iwContent,
    removable : iwRemoveable
});

// 마커에 클릭이벤트를 등록합니다

kakao.maps.event.addListener(marker, 'click', function() {
      // 마커 위에 인포윈도우를 표시합니다
      infowindow.open(map, marker);  
});

//지도에 클릭 이벤트를 등록합니다
//지도를 클릭하면 마지막 파라미터로 넘어온 함수를 호출합니다
kakao.maps.event.addListener(map, 'click', function(mouseEvent) {        
    
    // 클릭한 위도, 경도 정보를 가져옵니다 
    var latlng = mouseEvent.latLng; 
    console.log(latlng);
    
    // 마커 위치를 클릭한 위치로 옮깁니다
    marker.setPosition(latlng);
    
    var message = '클릭한 위치의 위도는 ' + latlng.getLat() + ' 이고, ';
    message += '경도는 ' + latlng.getLng() + ' 입니다';
    
    var resultDiv = document.getElementById('clickLatlng'); 
    resultDiv.innerHTML = message;
    infowindow.close();

    
});

function saveMarker(){
	var latLng = document.getElementById('clickLatlng').innerHTML;
	console.log(latLng);
	var abc = latLng.split('');
	var def = '';
	for (var i=0;i<abc.length;i++){
		if(parseInt(abc[i])||abc[i]=='.'||abc[i]=='\,'||abc[i]==0){
			def += (abc[i]+'');
		}
	}
	var ghi=def.split(',');
	console.log(def);
	var img = document.querySelector("#imgAttach");
	var title = document.querySelector("#title");
	var category = document.querySelector("#category");
	var content = document.querySelector("#content");
	
	var formData = new FormData();
	formData.append('pmTitle', title.value);
	formData.append('pmContent', content.value);
	formData.append('pmCategory', category.value);
	if(img.files[0]){
	formData.append('pmFileItem', img.files[0]);
	}
	formData.append('pmLatitude', ghi[0]);
	formData.append('pmLongitude', ghi[1]);
	console.log(formData);
	console.log(img.files[0]);
	
	var xhr = new XMLHttpRequest();
	xhr.open('POST', '/pmap/insert2');
	xhr.onreadystatechange = function() {
		if (xhr.readyState == xhr.DONE) {
			if (xhr.status == 200) {
				console.log(xhr.responseText);
				var msg = JSON.parse(xhr.responseText);
				alert(msg.msg);
				infowindow.close();
				location.href='/views/kakaomap/marker-onclick2';
				infowindow = new kakao.maps.InfoWindow({
				    content : iwContent,
				    removable : iwRemoveable
				});
				
			}
		}
	}
	xhr.send(formData);
}


window.onload = function(){
	var xhr = new XMLHttpRequest();
	xhr.open('GET', '/pmap/list2');
	xhr.onreadystatechange = function() {
		if (xhr.readyState == xhr.DONE) {
			if (xhr.status == 200) {
				function makeOverListener(map, marker, infowindow) {
				    return function() {
				        infowindow.open(map, marker);
				    };
				}
				function makeOutListener(infowindow) {
				    return function() {
				        infowindow.close();
				    };
				}
				var privateMapList = JSON.parse(xhr.responseText);
				var markers = [];
				for (var i=0;i<privateMapList.length;i++){
					var pmLat = privateMapList[i].pmLatitude;
					var pmLng = privateMapList[i].pmLongitude;
					console.log(pmLat);
					console.log(pmLng);
					var position = new kakao.maps.LatLng(pmLat, pmLng);
					var marker = new kakao.maps.Marker({
				        position: position,
				    });
					console.log(marker.getPosition);
					console.log(pmLat);
					console.log(pmLng);
					markers.push(marker);
					markers[i].setMap(map); 
					var upContent ='<div class="container">'+	
					'<div class="overlaybox">'+
					'<div class="placeName">'+
						'<label id="whiteCol">'+
							'작성양식'+
						'</label>'+
						'<input type="hidden" class="form-control" id="pmnum"	aria-describedby="basic-addon3" value=' + privateMapList[i].pmNum + '>'+
						'<input type="hidden" class="form-control" id="munum"	aria-describedby="basic-addon3" value=' + privateMapList[i].muNum + '>'+
						'<input type="text" class="form-control" id="title"	aria-describedby="basic-addon3" value=' + privateMapList[i].pmTitle + '>'+
					'</div>'+
					'<div class="first">'+
						'<div class="center-block">'+
							'<div class="triangle text">'+
								'<div class="filebox">'+
									'<img id="LoadImg" width="277px" height="150px"/>'+
									'<input type="file" id="imgAttach" name="imgAttach" onchange="LoadImg(this);">'+
									'<span>이미지 업로드</span>'+
								'</div>'+
							'</div>'+
						'</div>'+
					'</div>'+
					'<label id="whiteCol">카테고리 : </label>'+
					'<select class="form-control" id="category">'+
						'<option>종류를 골라주세요</option>'+
						'<option value="음식점">음식점</option>'+
						'<option value="병원">병원</option>'+
						'<option value="술집">술집</option>'+
					'</select>'+
					'<HR>'+
					'<textarea class="form-control" id="content" aria-describedby="basic-addon3" style="resize: none;">' + privateMapList[i].pmContent + '</textarea>'+
					'<HR>'+
					'<button id="tButton" class="btn btn-dark" onclick="updateMarker()">수정</button>'+
					'<button id="tButton" class="btn btn-dark" onclick="deleteMarker()">삭제</button>'+
					'</div>'+
					'</div>';
					
					var infowindow = new kakao.maps.InfoWindow({
				        content: upContent, // 인포윈도우에 표시할 내용
				        removable : iwRemoveable
				    });
					kakao.maps.event.addListener(marker, 'mouseover', makeOutListener(infowindow));
					kakao.maps.event.addListener(marker, 'click', makeOverListener(map, marker, infowindow));
					
				}
			}
		}
	}
	xhr.send();
}


function updateMarker(){
	var num = document.querySelector("#pmnum");
	var img = document.querySelector("#imgAttach");
	var title = document.querySelector("#title");
	var category = document.querySelector("#category");
	var content = document.querySelector("#content");
	
	var formData = new FormData();
	formData.append('pmNum', num.value);
	formData.append('pmTitle', title.value);
	formData.append('pmContent', content.value);
	formData.append('pmCategory', category.value);
	if(img.files[0]){
	formData.append('pmFileItem', img.files[0]);
	}
	console.log(formData);
	console.log(img.files[0]);
	
	var xhr = new XMLHttpRequest();
	xhr.open('PUT', '/pmap/update2');
	xhr.onreadystatechange = function() {
		if (xhr.readyState == xhr.DONE) {
			if (xhr.status == 200) {
				console.log(xhr.responseText);
				var msg = JSON.parse(xhr.responseText);
				alert(msg.msg);
				infowindow.close();
				location.href='/views/kakaomap/marker-onclick2';
				infowindow = new kakao.maps.InfoWindow({
				    content : iwContent,
				    removable : iwRemoveable
				});		
			}
		}
	}
	xhr.send(formData);
}

function deleteMarker(){
	var pmNum = {
			pmNum:parseInt(document.querySelector("#pmnum").value)
	}
	console.log(pmNum)
	
	pmNum = JSON.stringify(pmNum)
	console.log(pmNum)
	$.ajax({
		url:'/pmap/delete2',
		method:'DELETE',
		data:pmNum,
		beforeSend : function(xhr) {
			xhr.setRequestHeader('Content-type',
					'application/json;charset=utf-8')
		},
		success:function(res){
			if(res.result = 'true'){
				alert('삭제되었습니다.');
				location.href='/views/kakaomap/marker-onclick2';
			}else{
				alert('삭제가 제대로 이루어지지 않았습니다.');
			}
		},
		error:function(res){
		}
	})
}
</script>
<script>
		function LoadImg(value){
			if(value.files && value.files[0]){
				var reader = new FileReader();
				reader.onload = function(e){
					$('#LoadImg').attr('src',e.target.result);
				}
				reader.readAsDataURL(value.files[0]);
			}
		}
	</script>
</body>
</html>