<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Insert title here</title>
<script src="https://code.jquery.com/jquery-3.4.1.min.js"
	integrity="sha256-CSXorXvZcTkaix6Yvo6HppcZGetbYMGWSFlBw8HfCJo="
	crossorigin="anonymous"></script>
</head>
<body>
	<div id="map" style="width: 100%; height: 350px;"></div>
	<p>
		<em>지도를 클릭해주세요!</em>
	</p>
	<div id="clickLatlng"></div>
	<button id="tButton">원위치</button>

	<script type="text/javascript"
		src="//dapi.kakao.com/v2/maps/sdk.js?appkey=a3fdc8c4b9ff7aa04c7acecbe9c1b0d9"></script>
	<script>
		var lat;
		var lng;
		function loadPost(abc) {
			return new Promise(function(resolve, reject) {

				if (!!navigator.geolocation)

				{
					navigator.geolocation.getCurrentPosition(
							function(position) {

								lat = position.coords.latitude;

								lng = position.coords.longitude;
								resolve(position);
							}, errorCallback);

				}

				else

				{

					alert("이 브라우저는 Geolocation를 지원하지 않습니다");

				}
			})
		}

		function errorCallback(error)

		{

			alert(error.message);

		}
		/* function abc(pl1, pl2) {
			console.log(pl1)

			var mapcenter = new kakao.maps.LatLng(lat, lng);

			var mapContainer = document.getElementById('map'), // 지도를 표시할 div 
			mapOption = {
				center : mapcenter, // 지도의 중심좌표
				level : 3
			// 지도의 확대 레벨
			};

			var map = new kakao.maps.Map(mapContainer, mapOption); // 지도를 생성합니다

			// 지도를 클릭한 위치에 표출할 마커입니다
			var marker = new kakao.maps.Marker({
				// 지도 중심좌표에 마커를 생성합니다 
				position : map.getCenter()
			});
			// 지도에 마커를 표시합니다
			marker.setMap(map);
			kakao.maps.event.addListener(map, 'click', function(mouseEvent) {

				// 클릭한 위도, 경도 정보를 가져옵니다 
				var latlng = mouseEvent.latLng;

				// 마커 위치를 클릭한 위치로 옮깁니다
				marker.setPosition(latlng);

				var message = '클릭한 위치의 위도는 ' + latlng.getLat() + ' 이고, ';
				message += '경도는 ' + latlng.getLng() + ' 입니다';

				var resultDiv = document.getElementById('clickLatlng');
				resultDiv.innerHTML = message;

			});

		} */

		var ddd = await loadPost();
		console.log(ddd.coords.latitude);

		
		abc();

		//loadPost(abc);

		// 지도에 클릭 이벤트를 등록합니다
		// 지도를 클릭하면 마지막 파라미터로 넘어온 함수를 호출합니다
	</script>
</body>
</html>