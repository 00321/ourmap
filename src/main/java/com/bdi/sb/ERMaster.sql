SET SESSION FOREIGN_KEY_CHECKS=0;

/* Drop Tables */

DROP TABLE IF EXISTS public_map;
DROP TABLE IF EXISTS private_map;
DROP TABLE IF EXISTS map_user;




/* Create Tables */

CREATE TABLE map_user
(
	mu_num int(11) NOT NULL AUTO_INCREMENT,
	mu_id varchar(20) NOT NULL,
	mu_pwd varchar(50) NOT NULL,
	mu_name varchar(10) NOT NULL,
	mu_addr varchar(100) NOT NULL,
	mu_age int(11) NOT NULL,
	mu_gender varchar(10) NOT NULL,
	mu_email varchar(50) NOT NULL,
	mu_authkey varchar(30) NOT NULL,
	mu_authstatus int(2) DEFAULT 0 NOT NULL,
	PRIMARY KEY (mu_num),
	UNIQUE (mu_num),
	UNIQUE (mu_id)
);


CREATE TABLE private_map
(
	mu_num int(11) NOT NULL,
	pm_num int(11) NOT NULL AUTO_INCREMENT,
	pm_latitude float NOT NULL,
	pm_longitude float NOT NULL,
	pm_title varchar(100) NOT NULL,
	pm_content text,
	pm_file varchar(30),
	pm_category varchar(30) NOT NULL,
	PRIMARY KEY (pm_num),
	UNIQUE (pm_num)
);


CREATE TABLE public_map
(
	mu_num int(11) NOT NULL,
	pm_num int(11) NOT NULL,
	bm_num int(11) NOT NULL AUTO_INCREMENT,
	bm_latitude float NOT NULL,
	bm_longitude float NOT NULL,
	bm_title varchar(100) NOT NULL,
	bm_content text,
	bm_file varchar(30),
	bm_category varchar(30) NOT NULL,
	bm_like int DEFAULT 0 NOT NULL,
	PRIMARY KEY (bm_num),
	UNIQUE (pm_num),
	UNIQUE (bm_num)
);



/* Create Foreign Keys */

ALTER TABLE private_map
	ADD FOREIGN KEY (mu_num)
	REFERENCES map_user (mu_num)
	ON UPDATE RESTRICT
	ON DELETE RESTRICT
;


ALTER TABLE public_map
	ADD FOREIGN KEY (mu_num)
	REFERENCES map_user (mu_num)
	ON UPDATE RESTRICT
	ON DELETE RESTRICT
;


ALTER TABLE public_map
	ADD FOREIGN KEY (pm_num)
	REFERENCES private_map (pm_num)
	ON UPDATE RESTRICT
	ON DELETE RESTRICT
;



