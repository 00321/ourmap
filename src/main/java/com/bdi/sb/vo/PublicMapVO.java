package com.bdi.sb.vo;

import org.apache.ibatis.type.Alias;
import org.springframework.web.multipart.MultipartFile;

import lombok.Data;

@Data
@Alias("bm")
public class PublicMapVO {
	private Integer muNum;
	private String muName;
	private Integer pmNum;
	private Integer bmNum;
	private String bmLatitude;
	private String bmLongitude;
	private String bmTitle;
	private String bmContent;
	private String bmFile;
	private MultipartFile bmFileItem;
	private String bmCategory;
	private String bmCategoryName;
	private String bmRoadad;
	private String bmOldad;
	private Integer bmLike;
}
