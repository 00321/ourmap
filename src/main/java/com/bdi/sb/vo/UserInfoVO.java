package com.bdi.sb.vo;

import org.apache.ibatis.type.Alias;

import lombok.Data;

@Data
@Alias("mu")
public class UserInfoVO {
	private Integer muNum;
	private String muId;
	private String muPwd;
	private String muName;
	private String muAddr;
	private Integer muAge;
	private String muGender;
	private String muEmail;	
}
