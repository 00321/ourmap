package com.bdi.sb.vo;

import org.apache.ibatis.type.Alias;
import org.springframework.web.multipart.MultipartFile;

import lombok.Data;

@Data
@Alias("pm")
public class PrivateMapVO {
	private Integer muNum;
	private String muName;
	private Integer pmNum;
	private String pmLatitude;
	private String pmLongitude;
	private String pmTitle;
	private String pmContent;
	private String pmFile;
	private MultipartFile pmFileItem;
	private String pmCategory;
	private String pmCategoryName;
	private String pmRoadad;
	private String pmOldad;
}
