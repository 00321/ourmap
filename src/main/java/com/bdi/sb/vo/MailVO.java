package com.bdi.sb.vo;

import lombok.Data;

@Data
public class MailVO {
	private String email;
}
