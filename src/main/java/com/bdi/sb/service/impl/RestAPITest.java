package com.bdi.sb.service.impl;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import com.bdi.sb.vo.PrivateMapVO;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;


@RestController
public class RestAPITest {
	private List<PrivateMapVO> lPvo = new ArrayList<PrivateMapVO>();
	
	private ObjectMapper om = new ObjectMapper().configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
	
	@GetMapping("/keyword/search")
	public Map<String,Object> keyword(String latitude, String longitude, String keyword) throws Exception{
		String text = URLEncoder.encode(keyword,"UTF-8");
		URL url = new URL("https://dapi.kakao.com/v2/local/search/keyword.json?y="+latitude+
		"&x="+longitude+"&radius=20000&query="+text);
//		URL url = new URL("https://dapi.kakao.com/v2/local/search/k.json?y=37.3595305941092&x=127.105017634512&radius=20000&category_group_code=MT1");
		HttpURLConnection hc = (HttpURLConnection)url.openConnection();
		hc.setRequestMethod("GET");
		hc.setRequestProperty("Content-Type", "application/x-www-form-urlencoded; charset=UTF-8");
		hc.setRequestProperty("Authorization", "KakaoAK e7058753121dfc6c096730d541e3db75");
		hc.setDoOutput(true); //parameter를 보내려면 이걸 true로 해줘야 됨
		DataOutputStream dos = new DataOutputStream(hc.getOutputStream());
		dos.flush();
		dos.close();
		
		InputStreamReader isr = new InputStreamReader(hc.getInputStream(), "UTF-8");
		BufferedReader br = new BufferedReader(isr);
		StringBuffer res = new StringBuffer();
		String str = null;
		while((str=br.readLine())!=null) {
			res.append(str);
		}
		br.close();
		Map<String,Object> keywordResponse = om.readValue(res.toString(), Map.class);		
		List<Map<String,Object>> documents = (List<Map<String, Object>>) keywordResponse.get("documents");
		Map<String,Object> result = new HashMap<String,Object>();
		result.put("documents", documents);
		for (int i=0;i<documents.size();i++) {
			PrivateMapVO pvo = new PrivateMapVO();
			Map<String,Object> tempMap = new HashMap<String, Object>();
			tempMap=documents.get(i);
			pvo.setPmTitle((String) tempMap.get("place_name"));
			pvo.setPmFile((String) tempMap.get("place_url"));
			pvo.setPmLatitude((String) tempMap.get("y"));
			pvo.setPmLongitude((String) tempMap.get("x"));
			lPvo.add(pvo);
		}
		if(documents.size()<1) {
			url = new URL("https://dapi.kakao.com/v2/local/search/address.json?query="+text);
			hc = (HttpURLConnection)url.openConnection();
			hc.setRequestMethod("GET");
			hc.setRequestProperty("Authorization", "KakaoAK e7058753121dfc6c096730d541e3db75");
			hc.setDoOutput(true); //parameter를 보내려면 이걸 true로 해줘야 됨
			dos = new DataOutputStream(hc.getOutputStream());
			dos.flush();
			dos.close();
			isr = new InputStreamReader(hc.getInputStream(), "UTF-8");
			br = new BufferedReader(isr);
			res = new StringBuffer();
			str = null;
			while((str=br.readLine())!=null) {
				res.append(str);
			}
			br.close();
			keywordResponse = om.readValue(res.toString(), Map.class);
			System.out.println(keywordResponse);
		}
		
		System.out.println(documents);
		return result;
	}
	
	public static void main(String[] args) {
		RestAPITest rat = new RestAPITest();
		try {
			rat.keyword("37.4915792","126.8788277","카페");
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
