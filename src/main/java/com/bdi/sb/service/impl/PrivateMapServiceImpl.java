package com.bdi.sb.service.impl;

import java.awt.Image;
import java.io.IOException;
import java.net.URL;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.imageio.ImageIO;
import javax.servlet.http.HttpSession;

import org.apache.commons.io.FilenameUtils;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import com.amazonaws.SdkClientException;
import com.amazonaws.services.s3.model.ObjectMetadata;
import com.bdi.sb.config.CredentialsConfig;
import com.bdi.sb.mapper.PrivateMapMapper;
import com.bdi.sb.service.PrivateMapService;
import com.bdi.sb.vo.PrivateMapVO;
import com.bdi.sb.vo.UserInfoVO;

@Service
public class PrivateMapServiceImpl implements PrivateMapService {

	@Resource
	PrivateMapMapper pmm;
	@Resource
	CredentialsConfig cc;

	public Map<String, Object> insertPrivateMap(PrivateMapVO pvo, HttpSession session) {
		UserInfoVO mapU = (UserInfoVO) session.getAttribute("mapS");
		pvo.setMuNum(mapU.getMuNum());
		Map<String, Object> rMap = new HashMap<>();
		MultipartFile mf = pvo.getPmFileItem();
		if (mf != null) {
			String extName = FilenameUtils.getExtension(mf.getOriginalFilename());
			String fileName = System.nanoTime() + "." + extName;
			pvo.setPmFile(fileName);
			int cnt = pmm.insertPrivateMap(pvo);
			if (cnt == 1) {
				rMap.put("msg", "성공");
				ObjectMetadata omd = new ObjectMetadata();
				omd.setContentType(pvo.getPmFileItem().getContentType());
				omd.setContentLength(pvo.getPmFileItem().getSize());
				try {
					cc.getS3Client().putObject("ourmapimg", "img/" + fileName, pvo.getPmFileItem().getInputStream(),
							omd);
				} catch (SdkClientException | IOException e) {
					e.printStackTrace();
					Map<String,Integer> deleteImg = new HashMap<>();
					deleteImg.put("pmNum", pvo.getPmNum());
					pmm.deletePrivateMap(deleteImg);
				}
				System.out.println("파일네임이 찍혀야 함 : " + fileName);
				return rMap;
			}
		}
		rMap.put("msg", "실패");
		return rMap;
	}

	@Override
	public List<PrivateMapVO> selectPrivateMapList(PrivateMapVO pvo, HttpSession session) {
		return pmm.selectPrivateMapList(pvo);
	}

	@Override
	public PrivateMapVO selectPrivateMap(PrivateMapVO data, HttpSession session) {
		return pmm.selectPrivateMap(data);
	}

	@Override
	public Map<String, Object> updatePrivateMap(PrivateMapVO pvo, HttpSession session) {
		Map<String, Object> rMap = new HashMap<>();
		int cnt = pmm.updatePrivateMap(pvo);
		rMap.put("msg", "수정 실패");
		if (cnt == 1) {
			rMap.put("msg", "수정 성공");
		}
		return rMap;
	}

	@Override
	public Map<String, Object> deletePrivateMap(Map<String, Integer> pmNum, HttpSession session) {
		Map<String, Object> rMap = new HashMap<>();
		int cnt = pmm.deletePrivateMap(pmNum);
		rMap.put("msg", "삭제 실패");
		if (cnt == 1) {
			rMap.put("msg", "삭제 성공");
		}
		return rMap;
	}
	
	@Override
	public Map<String, Object> downloadPublicMap(PrivateMapVO pvo, HttpSession session) {
		Map<String, Object> rMap = new HashMap<>();
		rMap.put("msg", "실패");
		int cnt = pmm.downloadPublicMap(pvo);
		if (cnt == 1) {
			rMap.put("msg", "성공");
		}
		return rMap;
	}
}
