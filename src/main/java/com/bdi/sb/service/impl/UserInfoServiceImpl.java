package com.bdi.sb.service.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpSession;

import org.springframework.stereotype.Service;

import com.bdi.sb.mapper.UserInfoMapper;
import com.bdi.sb.service.UserInfoService;
import com.bdi.sb.vo.UserInfoVO;

@Service
public class UserInfoServiceImpl implements UserInfoService {
	
	@Resource
	private UserInfoMapper uim;

	@Override
	public List<UserInfoVO> selectUserInfoList(UserInfoVO muv) {
		return uim.selectUserInfoList(muv);
	}
	
	@Override
	public UserInfoVO selectUserInfo(UserInfoVO muv) {
		return uim.selectUserInfo(muv);
	}
	
	@Override
	public Map<String,Object> loginMapUser(UserInfoVO muv,HttpSession hs) {
		Map<String,Object> rsMap = new HashMap<>();
		UserInfoVO mapU = uim.loginMapUser(muv);
		if(mapU==null) {
			rsMap.put("result", "0");
		} else {
			rsMap.put("mapuser", mapU);
			rsMap.put("result", "1");
			hs.setAttribute("mapS", mapU);
		}
		return rsMap;
	}

	@Override
	public Map<String, String> signupMapUser(UserInfoVO muv) {
		Map<String,String> rMap = new HashMap<>();
		int result = uim.insertMapUser(muv);
			if(result == 0) {
				rMap.put("result", "false");
			} else {
				rMap.put("result", "true");
				rMap.put("idcheck", "중복된 아이디입니다.");
			}
		return rMap;
	}

	@Override
	public Map<String, String> updateMapUser(UserInfoVO muv) {
		Map<String,String> rMap = new HashMap<>();
		int result = uim.updateMapUser(muv);
		if(result == 0) {
			rMap.put("result", "false");
		} else {
			rMap.put("result", "true");
		}
		return rMap;
	}

	@Override
	public Map<String, String> deleteMapUser(UserInfoVO muv) {
		Map<String,String> rMap = new HashMap<>();
		int result = uim.deleteMapUser(muv);
		if(result == 0) {
			rMap.put("result", "false");
		} else {
			rMap.put("result", "true");
		}
		return rMap;
	}

	

}
