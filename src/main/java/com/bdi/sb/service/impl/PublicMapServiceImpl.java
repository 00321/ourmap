package com.bdi.sb.service.impl;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.StandardCopyOption;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpSession;

import org.apache.commons.io.FilenameUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import com.bdi.sb.mapper.PublicMapMapper;
import com.bdi.sb.service.PublicMapService;
import com.bdi.sb.vo.PublicMapVO;

@Service
public class PublicMapServiceImpl implements PublicMapService {
	
	@Value("${save.file.path")
	private String saveFilePath;
	@Resource
	PublicMapMapper bmm;
	
	@Override
	public Map<String, Object> insertPublicMap(PublicMapVO bvo, HttpSession session) {
		/*
		 * String path = session.getServletContext().getRealPath(""); path =
		 * path.substring(0,path.lastIndexOf("webapp")); path += "/resources/img";
		 * System.out.println(path);
		 */
		
		Map<String, Object> rMap = new HashMap<>();
		/* MultipartFile mf = bvo.getBmFileItem();
		bvo.setMuNum((Integer)session.getAttribute("muNum"));
		bvo.setMuNum((Integer)session.getAttribute("puNum")); */
		/*
		 * if (mf != null) { String extName =
		 * FilenameUtils.getExtension(mf.getOriginalFilename()); String fileName = "/" +
		 * System.nanoTime() + "." + extName; bvo.setBmFile(fileName); String filePath =
		 * path + fileName; File targetFile = new File(filePath); try {
		 * Files.copy(mf.getInputStream(), targetFile.toPath(),
		 * StandardCopyOption.REPLACE_EXISTING); } catch (IOException e) {
		 * e.printStackTrace(); } }
		 */
		rMap.put("msg", "실패");
		int cnt = bmm.insertPublicMap(bvo);
		if (cnt == 1) {
			rMap.put("msg", "성공");
		}
		return rMap;
	}

	@Override
	public List<PublicMapVO> selectPublicMapList(PublicMapVO bvo, HttpSession session) {
		//bvo.setMuNum((Integer)session.getAttribute("muNum"));
		bvo.setMuNum(1);
		return bmm.selectPublicMapList(bvo);
	}

	@Override
	public PublicMapVO selectPublicMap(PublicMapVO bvo, HttpSession session) {
		return bmm.selectPublicMap(bvo);
	}

	@Override
	public Map<String, Object> updatePublicMap(PublicMapVO bvo, HttpSession session) {
		//bvo.setMuNum((Integer)session.getAttribute("muNum"));
		bvo.setMuNum(1);
		Map<String,Object> rMap = new HashMap<>();
		int cnt = bmm.updatePublicMap(bvo);
		rMap.put("msg","수정 실패");
		if(cnt==1) {
			rMap.put("msg","수정 성공");
		}
		return rMap;
	}

	@Override
	public Map<String, Object> deletePublicMap(PublicMapVO bvo, Map<String, Integer> bmNum, HttpSession session) {
		bvo.setMuNum((Integer)session.getAttribute("muNum"));
		Map<String,Object> rMap = new HashMap<>();
		int cnt = bmm.deletePublicMap(bmNum);
		rMap.put("msg","삭제 실패");
		if(cnt==1) {
			rMap.put("msg","삭제 성공");
		}
		return rMap;
	}
}
