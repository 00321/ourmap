package com.bdi.sb.service;

import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpSession;

import com.bdi.sb.vo.PublicMapVO;

public interface PublicMapService {
	
	public Map<String, Object> insertPublicMap(PublicMapVO bvo, HttpSession session);
	
	public List<PublicMapVO> selectPublicMapList(PublicMapVO bvo, HttpSession session);
	
	public PublicMapVO selectPublicMap(PublicMapVO bvo,HttpSession session);
	
	public Map<String, Object> updatePublicMap(PublicMapVO bvo, HttpSession session);
	
	public Map<String, Object> deletePublicMap(PublicMapVO bvo, Map<String,Integer> bmNum, HttpSession session);
}
