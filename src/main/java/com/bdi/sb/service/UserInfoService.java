package com.bdi.sb.service;


import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpSession;

import com.bdi.sb.vo.UserInfoVO;

public interface UserInfoService {
	
	public List<UserInfoVO> selectUserInfoList(UserInfoVO muv);
	public UserInfoVO selectUserInfo(UserInfoVO muv);
	public Map<String,Object> loginMapUser(UserInfoVO muv,HttpSession hs);
	public Map<String,String> signupMapUser(UserInfoVO muv);
	public Map<String,String> updateMapUser(UserInfoVO muv);
	public Map<String,String> deleteMapUser(UserInfoVO muv);
}
