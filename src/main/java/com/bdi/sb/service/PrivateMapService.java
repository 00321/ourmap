package com.bdi.sb.service;

import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpSession;

import com.bdi.sb.vo.PrivateMapVO;
import com.bdi.sb.vo.PublicMapVO;

public interface PrivateMapService {

	public Map<String, Object> insertPrivateMap(PrivateMapVO pvo, HttpSession session);
	
	public List<PrivateMapVO> selectPrivateMapList(PrivateMapVO pvo, HttpSession session);
	
	public PrivateMapVO selectPrivateMap(PrivateMapVO pvo,HttpSession session);
	
	public Map<String, Object> updatePrivateMap(PrivateMapVO pvo, HttpSession session);
	
	public Map<String, Object> deletePrivateMap(Map<String,Integer> pmNum, HttpSession session);
	
	public Map<String, Object> downloadPublicMap(PrivateMapVO pvo, HttpSession session);
}
