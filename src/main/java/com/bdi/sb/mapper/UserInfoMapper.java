package com.bdi.sb.mapper;

import java.util.List;

import org.mybatis.spring.annotation.MapperScan;

import com.bdi.sb.vo.UserInfoVO;

@MapperScan
public interface UserInfoMapper {

	public List<UserInfoVO> selectUserInfoList(UserInfoVO muv);
	public UserInfoVO selectUserInfo(UserInfoVO muv);
	public UserInfoVO loginMapUser(UserInfoVO muv);
	public int insertMapUser(UserInfoVO muv);
	public int updateMapUser(UserInfoVO muv);
	public int deleteMapUser(UserInfoVO muv);
}
