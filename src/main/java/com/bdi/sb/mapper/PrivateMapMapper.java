package com.bdi.sb.mapper;

import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpSession;

import org.springframework.stereotype.Component;

import com.bdi.sb.vo.PrivateMapVO;
import com.bdi.sb.vo.PublicMapVO;

@Component
public interface PrivateMapMapper {

	public List<PrivateMapVO> selectPrivateMapList(PrivateMapVO data);
	public PrivateMapVO selectPrivateMap(PrivateMapVO data);
	public int insertPrivateMap(PrivateMapVO data);
	public int updatePrivateMap(PrivateMapVO data);
	public int deletePrivateMap(Map<String, Integer> pmNum);
	public int downloadPublicMap(PrivateMapVO pvo);
}
