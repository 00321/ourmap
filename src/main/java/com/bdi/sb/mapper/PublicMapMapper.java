package com.bdi.sb.mapper;

import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Component;

import com.bdi.sb.vo.PublicMapVO;

@Component
public interface PublicMapMapper {
	public List<PublicMapVO> selectPublicMapList(PublicMapVO data);
	public PublicMapVO selectPublicMap(PublicMapVO data);
	public int insertPublicMap(PublicMapVO data);
	public int updatePublicMap(PublicMapVO data);
	public int deletePublicMap(Map<String, Integer> bmNum);
}
