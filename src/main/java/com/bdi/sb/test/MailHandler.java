package com.bdi.sb.test;

import java.io.File;
import java.io.UnsupportedEncodingException;

import javax.annotation.Resource;
import javax.mail.internet.MimeMessage;

import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;

import com.sun.xml.messaging.saaj.packaging.mime.MessagingException;

public class MailHandler {
	private JavaMailSender sender;
	  private MimeMessage message;
	  private MimeMessageHelper messageHelper;
	  //MailHandler의 생성자
	  public MailHandler(JavaMailSender jSender) throws 
	  MessagingException {
	    this.sender = jSender;
	    message = jSender.createMimeMessage();
	    try {
			messageHelper = new MimeMessageHelper(message, true, "UTF-8");
		} catch (javax.mail.MessagingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	    //MimeMessageHelper의 생성자 두번째 파라미터는 다수의 사람에게 보낼 수 있는 설정, 세번째는 기본 인코딩 방식
	  }
	  //이 이메일이 누구로부터 가는가.. 실제로 써본결과 그다지 중요하지 않은듯..잘  모르겠습니다.
	  public void setFrom(String email,String name) throws 
	  UnsupportedEncodingException, MessagingException {
	    try {
			messageHelper.setFrom(email, name);
		} catch (javax.mail.MessagingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	  }
	  //누구에게 보낼 것인가.. 보낼사람의 이메일
	  public void setTo(String email) throws MessagingException {
	    try {
			messageHelper.setTo(email);
		} catch (javax.mail.MessagingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	  }
	  //보낼때 제목
	  public void setSubject(String subject) throws MessagingException {
	    try {
			messageHelper.setSubject(subject);
		} catch (javax.mail.MessagingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	  }
	  //보낼 메일의 내용.. 두번째 파라미터는 html을 적용할 것인가 아닌가. true시 html형식으로 작성하면 html형식으로 보임..
	  public void setText(String text) throws MessagingException {
	    try {
			messageHelper.setText(text, true);
		} catch (javax.mail.MessagingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	  }
	  
	  //실제로 메일을 보내는 메서드..
	  public void send() {
	    try {
	       sender.send(message);
	    }catch(Exception e) {
	       e.printStackTrace();
	    }
	  }
	}
