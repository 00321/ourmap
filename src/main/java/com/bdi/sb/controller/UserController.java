package com.bdi.sb.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpSession;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

import com.bdi.sb.service.UserInfoService;
import com.bdi.sb.vo.UserInfoVO;

import lombok.extern.slf4j.Slf4j;

@RestController
@Slf4j
public class UserController {
	@Resource
	private UserInfoService uis; 

	@PostMapping("/mapuser/getList")
	public List<UserInfoVO> selectUserInfoList(UserInfoVO muv){
		return uis.selectUserInfoList(muv);
	}
	
	@PostMapping("/mapuser/selectOne")
	public UserInfoVO selectUserInfo(UserInfoVO muv) {
		return uis.selectUserInfo(muv);
	}
	
	@PostMapping("/mapuser/login")
	public Map<String,Object> loginMapUser(UserInfoVO muv,HttpSession hs){
		log.info("muv =>{}",muv);
		return uis.loginMapUser(muv,hs);
	}
	
	@PostMapping("/mapuser/logout")
	public Map<String,String> logoutMapUser(HttpSession hs){
		Map<String,String> rMap = new HashMap<>();
		hs.invalidate();
		rMap.put("msg", "로그아웃되었습니다");
		return rMap;
	}
	
	@PostMapping("/mapuser/signup")
	public Map<String,String> signup(UserInfoVO muv){
		log.info("muv => {}",muv);
		return uis.signupMapUser(muv);
	}
	
	@PostMapping("/mapuser/update")
	public Map<String,String> updateInfo(UserInfoVO muv){
		log.info("muv => {}",muv);
		return uis.updateMapUser(muv);
	}
	
	@PostMapping("/mapuser/delete")
	public Map<String,String> deleteUser(UserInfoVO muv){
		return uis.deleteMapUser(muv);
	}
}
