package com.bdi.sb.controller;

import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpSession;

import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.bdi.sb.service.impl.PublicMapServiceImpl;
import com.bdi.sb.vo.PublicMapVO;

import lombok.extern.slf4j.Slf4j;

@RestController
@Slf4j
public class PublicMapController {

	@Resource
	PublicMapServiceImpl bmsi;
	
	@PostMapping("/bmap/insert")
	public Map<String,Object> insertPublicMap(@ModelAttribute PublicMapVO data, HttpSession session){
		log.debug("data=>{}",data);
		return bmsi.insertPublicMap(data, session);
	}
	
	@GetMapping("/bmap/list")
	public List<PublicMapVO> selectPublicMapList(PublicMapVO data, HttpSession session){
		System.out.println("hihi");
		return bmsi.selectPublicMapList(data, session);
	}
	
	@GetMapping("/bmap/one")
	public PublicMapVO selectPublicMap(PublicMapVO data, HttpSession session) {
		return bmsi.selectPublicMap(data, session);
	}
	
	@PutMapping("/bmap/update")
	public Map<String,Object> updatePublicMap(@ModelAttribute PublicMapVO data, HttpSession session){
		return bmsi.updatePublicMap(data, session);
	}
	
	@DeleteMapping("/bmap/delete")
	public Map<String,Object> deletePublicMap(@RequestBody PublicMapVO bvo, Map<String,Integer> bmNum, HttpSession session){
		return bmsi.deletePublicMap(bvo, bmNum, session);
	}
}
