package com.bdi.sb.controller;

import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpSession;

import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.bdi.sb.service.impl.PrivateMapServiceImpl;
import com.bdi.sb.vo.PrivateMapVO;
import com.bdi.sb.vo.PublicMapVO;

import lombok.extern.slf4j.Slf4j;

@RestController
@Slf4j
public class PrivateMapController {

	@Resource
	PrivateMapServiceImpl pmsi;

	@PostMapping("/pmap/insert")
	public Map<String,Object> insertPrivateMap(@ModelAttribute PrivateMapVO data, HttpSession session) {
		log.debug("data=>{}",data);
		return pmsi.insertPrivateMap(data,session);
	}
	
	
	
	@PostMapping("/pmap/list")
	public List<PrivateMapVO> selectPrivateMapList(@RequestBody PrivateMapVO data, HttpSession session){
		log.debug("data=>{}",data);
		return pmsi.selectPrivateMapList(data,session);
	}
	
	@GetMapping("/pmap/one")
	public PrivateMapVO selectPrivateMap(PrivateMapVO data, HttpSession session){
		return pmsi.selectPrivateMap(data,session);
	}
	
	
	@PutMapping("/pmap/update")
	public Map<String,Object> updatePrivateMapList(@ModelAttribute PrivateMapVO data, HttpSession session){
		return pmsi.updatePrivateMap(data,session);
	}
	
	
	@DeleteMapping("/pmap/delete")
	public Map<String,Object> deletePrivateMapList(@RequestBody Map<String,Integer> pmNum, HttpSession session){
		return pmsi.deletePrivateMap(pmNum,session);
	}
	
	@PostMapping("/pmap/download")
	public Map<String,Object> downloadPublicMap(@ModelAttribute PrivateMapVO data, HttpSession session) {
		log.debug("data=>{}",data);
		return pmsi.downloadPublicMap(data,session);
	}
}
