package com.bdi.sb.controller;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;

import lombok.extern.slf4j.Slf4j;

@RestController
@Slf4j
public class TmapApiTest {
	
	private ObjectMapper om = new ObjectMapper().configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
	@Value("${tmap.key}")
	private String appKey;
	private String startX="126.57055401492185";
	private String startY="33.45062826196344";
	private String endX="126.55811677588761";
	private String endY="33.45565155847229";
	
	@GetMapping("/tMap")
	public Map<String, Object> tMapTest(String mode, String startX, String startY, String endX, String endY) throws Exception{
		String urlAddr = "";
		if(!mode.equals("transit")) {
			if (mode.equals("walk")) {
				String startPoint = URLEncoder.encode("출발지","UTF-8");
				String endPoint = URLEncoder.encode("도착지","UTF-8");
				urlAddr += "https://apis.openapi.sk.com/tmap/routes/pedestrian"
						+ "?appKey="+appKey+"&version=1"
						+ "&startX="+startX
						+ "&startY="+startY+"&endX="+endX+"&endY="+endY
						+ "&startName="+startPoint+"&endName="+endPoint+"&sort=index";
			}else {
				urlAddr +="http://apis.openapi.sk.com/tmap/routes?"
						+ "appKey="+appKey+"&version=1"
						+ "&tollgateFareOption=8&startX="+startX
						+ "&startY="+startY+"&endX="+endX+"&endY="+endY;
			}
			URL url = new URL(urlAddr);
			HttpURLConnection hc = (HttpURLConnection)url.openConnection();
			hc.setRequestMethod("GET");
			hc.setDoOutput(true);
			DataOutputStream dos = new DataOutputStream(hc.getOutputStream());
			dos.flush();
			dos.close();
			InputStreamReader isr = new InputStreamReader(hc.getInputStream(), "UTF-8");
			BufferedReader br = new BufferedReader(isr);
			StringBuffer res = new StringBuffer();
			String str = null;
			while((str=br.readLine())!=null) {
				res.append(str);
			}
			br.close();
			Map<String,Object> keywordResponse = om.readValue(res.toString(), Map.class);
			List<Object> features = (List<Object>) keywordResponse.get("features"); //features 전부 가져오기
			List<Object> coordinates = new ArrayList<Object>(); // 
			Map<String,Object> getMyResponse = new HashMap<String, Object>();
			Map<String,Object> getFirstFeatures = (Map<String,Object>)features.get(0); //처음properties에 전체정보 다 나와있음
			Map<String,Object> getProperties = (Map<String,Object>)getFirstFeatures.get("properties"); // properties가져오기(시간,거리,요금에대한 정보있음)
			getMyResponse.put("properties", getProperties);
			for (int i=0;i<features.size();i++) {
				Map<String,Object> test1 = (Map<String,Object>)features.get(i); //features내에 아이템 가져오기
				Map<String,Object> test2 = (Map<String, Object>) test1.get("geometry"); //geometry부분 가져오기
				List<Object> test3 = (List<Object>)test2.get("coordinates");
				if(test3.get(0) instanceof List<?>) {
					for (int j=0;j<test3.size();j++) {
						coordinates.add(test3.get(j));
					}
				}else coordinates.add(test3);
			}
			
			getMyResponse.put("coordinates", coordinates);
			return getMyResponse;
			
		}
		return null;
	}
	
	public static void main(String[] args) {
		TmapApiTest tat = new TmapApiTest();
		String mode = "walk";
		try {
//			System.out.println(tat.tMapTest(mode));
//			System.out.println(tat.gMapTest());
//			tat.tMapTest(mode);
		} catch (Exception e) {
			e.printStackTrace();
		}
		
	}
}
