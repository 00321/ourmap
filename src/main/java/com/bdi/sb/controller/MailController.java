package com.bdi.sb.controller;

import java.util.HashMap;
import java.util.Map;
import java.util.Random;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.scheduling.annotation.Async;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

import com.bdi.sb.vo.MailVO;

@RestController
public class MailController {

	@Autowired
	public JavaMailSender javaMailSender;
	
	@PostMapping("/user/MailTest")
	public Map<String,Object> sendTome(MailVO mv) {
		System.out.println(mv.getEmail());
		Map<String,Object> rMap = sendMail(mv.getEmail());
		return rMap;
	}
	
	@Async
	public Map<String,Object> sendMail(String email) {
		int randomNum = new Random().nextInt(10000)+1000;
		Map<String,Object> rMap = new HashMap<>();
		SimpleMailMessage simpleMessage = new SimpleMailMessage();
		simpleMessage.setFrom("ourmapcf@gmail.com"); // NAVER, DAUM, NATE일 경우 넣어줘야 함
		simpleMessage.setTo(email);
		simpleMessage.setSubject("이메일 인증");
		simpleMessage.setText("인증번호 : "+ randomNum);
		javaMailSender.send(simpleMessage);
		rMap.put("result", "true");
		rMap.put("num", randomNum);
		return rMap;
	}
}
