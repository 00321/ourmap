package com.bdi.sb;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.data.mongo.MongoDataAutoConfiguration;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.boot.autoconfigure.mongo.MongoAutoConfiguration;
import org.springframework.context.annotation.PropertySource;

@SpringBootApplication(exclude = {MongoAutoConfiguration.class, MongoDataAutoConfiguration.class})
@MapperScan({"com.bdi.sb.mapper"}) //이게 mapper xml파일이랑 interface로 만든 mapper(dao)부분을 연결시켜주는것
@PropertySource({"classpath:/env.properties"}) //이게 프로퍼티 파일 읽으라고 하는것
public class MymaptestApplication {

	public static void main(String[] args) {
		SpringApplication.run(MymaptestApplication.class, args);
	}

}
