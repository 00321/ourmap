<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Insert title here</title>
<!-- jQuery -->
<script src="https://code.jquery.com/jquery-3.4.1.min.js"
	integrity="sha256-CSXorXvZcTkaix6Yvo6HppcZGetbYMGWSFlBw8HfCJo="
	crossorigin="anonymous"></script>
<!-- jQuery END-->

<script src="/js/bootstrap.min.js"></script>
<!-- sidebar js, css -->
<link href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700,800,900"
	rel="stylesheet">
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
<link rel="stylesheet" href="/sidebar2/css/style.css">
<script src="/sidebar2/js/bootstrap.min.js"></script>
<script src="/sidebar2/js/jquery.min.js"
	type="2e8105826a4af43d8e85ace3-text/javascript"></script>
<script src="/sidebar2/js/popper.js"
	type="2e8105826a4af43d8e85ace3-text/javascript"></script>
<script src="/sidebar2/js/main.js"
	type="2e8105826a4af43d8e85ace3-text/javascript"></script>
<script src="https://ajax.cloudflare.com/cdn-cgi/scripts/7089c43e/cloudflare-static/rocket-loader.min.js" data-cf-settings="2e8105826a4af43d8e85ace3-|49" defer="">
</script>
<!-- sidebar js,css end -->
<!-- fontawsome -->
<script src="https://kit.fontawesome.com/86fb79723e.js"
	crossorigin="anonymous"></script>
<!-- fontawsome end -->
<style>
#mainContent {
	padding: 62px 0 0 0;
}

#search {
	display: flex;
}

#pinMenu {
	position: fixed;
	bottom: 50px;
	right: 50px;
	border-radius: 30px;
}
.id-text-center{
	text-align:center;
}
#baccol{
	background-color: #343a40;
} 
.col-form-label{
	color:white;
}
.modal-title{
	color:white;
}
</style>
</head>
<body>
<!-- navbar START -->
	<nav class="navbar navbar-expand-md navbar-dark fixed-top bg-dark">
		<a class="navbar-brand" href="#">Fixed navbar</a>
		<button class="navbar-toggler" type="button" data-toggle="collapse"
			data-target="#navbarCollapse" aria-controls="navbarCollapse"
			aria-expanded="false" aria-label="Toggle navigation">
			<span class="navbar-toggler-icon"></span>
		</button>
		<div class="collapse navbar-collapse" id="navbarCollapse">
			<ul class="navbar-nav mr-auto">
				<li class="nav-item active"><a class="nav-link" href="#">Home
						<span class="sr-only">(current)</span>
				</a></li>
				<li class="nav-item"><a class="nav-link" href="#">Link</a></li>
				<li class="nav-item"><a class="nav-link disabled" href="#">Disabled</a>
				</li>
				<li class="nav-item">
					<div id="search">
						<input class="form-control" type="text" placeholder="Search"
							aria-label="Search">
						<button class="btn btn-outline-success my-2 my-sm-0">Search</button>
					</div>
				</li>
			</ul> 
				 <button type="button" class="btn btn-outline-success" data-toggle="modal" data-target="#form">login</button>
		</div>
		
	</nav>
	<!-- navbar END -->

<!-- modal login start -->
<div class="modal fade" id="form" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document" >
    <div class="modal-content" id="baccol">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">로그인</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
          <div class="form-group">
            <label for="id" class="col-form-label">아이디:</label>
            <input type="text" class="form-control" id="id">
          </div>
          <div class="form-group">
            <label for="pwd" class="col-form-label">비밀번호:</label>
            <input type="password" class="form-control" id="pwd">
          </div>
      </div>
      <div class="modal-footer">
        <button class="btn btn-primary" data-dismiss="modal"  id="lsbt">로그인</button>
        <button class="btn btn-success" onclick="onpage('/views/front/user/signup')">회원가입</button>
      </div>
    </div>
  </div>
</div>
 <!-- modal login end -->

<!-- sidebar start -->
	<div class="wrapper d-flex align-items-stretch" id="mainContent">
		<nav id="sidebar">
			<div class="custom-menu">
				<button type="button" id="sidebarCollapse" class="btn btn-primary">
					<i class="fa fa-bars"></i> <span class="sr-only">Toggle Menu</span>
				</button>
			</div>
			<h1>
				<a href="index.html" class="logo">Project Name</a>
			</h1>
			<ul class="list-unstyled components mb-5">
				<li class="active"><a href="#"><span
						class="fa fa-home mr-3"></span> Homepage</a></li>
				<li><a href="#"><span class="fa fa-user mr-3"></span> 나만의
						지도</a></li>
				<li><a href="#"><span class="fa fa-sticky-note mr-3"></span>
						공유 지도</a></li>
				<li><a href="#"><span class="fa fa-sticky-note mr-3"></span>
						아무거나</a></li>
				<li><a href="#"><span class="fa fa-paper-plane mr-3"></span>
						넣을것</a></li>
				<li><a href="#"><span class="fa fa-paper-plane mr-3"></span>
						생각해보자</a></li>
			</ul>
		</nav>
<!-- sidebar end -->
		<div id="content" class="p-4 p-md-5 pt-5">
			<h2 class="mb-4">지도</h2>
			<p>지도가 들어갈 곳입니다</p>
			<p>지도지도</p>
			<input type="text" class="form-control">
			<button id="pinMenu" class="btn btn-outline-dark">
				<i class="far fa-arrow-alt-circle-left fa-3x"></i>
			</button>
		</div>
	</div>
	<script>
		function onpage(f) {
			location.href = f;
		}
		
	</script>
</body>
</html>